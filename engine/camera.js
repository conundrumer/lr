Math.sign = Math.sign || function(x) {
  x = +x; // convert to a number
  if (x === 0 || isNaN(x)) {
    return x;
  }
  return x > 0 ? 1 : -1;
}

// function pointInsideEllipse (c, e) {
//   var x = c.x / e.rx
//   var y = c.y / e.ry
//   return x * x + y * y <= 1
// }
// MAY FAIL FOR POINTS INSIDE ELLIPSE
function closestPointOnEllipse (c, e, ITERATIONS) {
  ITERATIONS = ITERATIONS || 2 // >= 2 is good enough
  var t = Math.atan2(e.rx * c.y, e.ry * c.x)

  var X = c.x * e.rx
  var Y = c.y * e.ry
  var D = (e.rx * e.rx - e.ry * e.ry)

  for (var i = 0; i < ITERATIONS; i++) {
    // netwon's method: t = t - f(t) / f'(t)
    var cos_t = Math.cos(t)
    var sin_t = Math.sin(t)
    t -= (D * cos_t * sin_t - X * sin_t + Y * cos_t) / (D * (cos_t * cos_t - sin_t * sin_t) - X * cos_t - Y * sin_t)
  }
  return {
    x: e.rx * Math.cos(t),
    y: e.ry * Math.sin(t)
  }
}

function softBound (t, pull, push) {
  if (pull >= 1) return 0
  let slope = 1 - pull
  let threshold = (1 + pull) * (1 - push)
  let u
  if (t > threshold) {
    u = 1
    if (push > 0) {
      let c = 1 - slope * threshold
      let k = 1 - c * Math.pow(Math.exp(slope / c), -t + threshold)
      u *= k
    }
  } else {
    u = t * slope
  }
  return u
}

// function hardEllipseBound (p, e) {
//   if (pointInsideEllipse(p, e)) {
//     return p
//   } else {
//     return closestPointOnEllipse(p, e)
//   }
// }

// function ellipseBound (p, e, {pull, push}) {
//   // (x,y) is point on ellipse, t is relative distance
//   let x = p.x / e.rx
//   let y = p.y / e.ry
//   let t = x * x + y * y
//   if (t <= 1) {
//     t = Math.sqrt(t)
//     x = p.x / t
//     y = p.y / t
//   } else {
//     ({x, y} = closestPointOnEllipse(p, e))
//     let dx = p.x - x
//     let dy = p.y - y
//     t = 1 + Math.sqrt(dx * dx + dy * dy) / Math.sqrt(x * x + y * y)
//   }
//   let u = softBound(t, pull, push)
//   return {x: x * u, y: y * u}
// }

function ovalBound (p, {rx, ry}, {pull, push}) {
  let dr = push * ((rx > ry) ? ry : rx)
  rx -= dr
  ry -= dr
  let x = p.x / rx
  let y = p.y / ry
  // (x,y) is point on inner ellipse, t is relative distance from center
  let t = x * x + y * y
  let innerLength, distance, outerDistance, dx, dy
  if (t <= 1) {
    t = Math.sqrt(t)
    x = p.x / t
    y = p.y / t
    innerLength = Math.sqrt(x * x + y * y)
    distance = Math.sqrt(p.x * p.x + p.y * p.y)
  } else {
    ({x, y} = closestPointOnEllipse(p, {rx, ry}))
    dx = p.x - x
    dy = p.y - y
    innerLength = Math.sqrt(x * x + y * y)
    outerDistance = Math.sqrt(dx * dx + dy * dy)
    distance = outerDistance + innerLength
  }
  let totalLength = dr + innerLength
  let pushThreshold = dr / totalLength
  t = distance / totalLength
  let u = softBound(t, pull, pushThreshold)
  if (u <= (1 - pushThreshold)) {
    return {
      x: x * u * totalLength / innerLength,
      y: y * u * totalLength / innerLength
    }
  } else {
    let k = (u - (1 - pushThreshold)) / pushThreshold
    return {
      x: dx * k * dr / outerDistance + x,
      y: dy * k * dr / outerDistance + y
    }
  }
}

// function hardCircleBound ({x, y}, {rx, ry}) {
//   let r = Math.min(rx, ry)
//   let d = Math.sqrt(x * x + y * y)
//   if (d > r) {
//     x *= r / d
//     y *= r / d
//   }
//   return {x, y}
// }

function circleBound ({x, y}, {rx, ry}, {pull, push}) {
  let r = Math.min(rx, ry)
  let d = Math.sqrt(x * x + y * y)
  let u = softBound(d / r, pull, push)
  let k = u * r / d
  return {x: x * k, y: y * k}
}

function rectBound ({x, y}, {rx, ry}, {pull, push}) {
  return {
    x: Math.sign(x) * rx * softBound(Math.abs(x) / rx, pull, push),
    y: Math.sign(y) * ry * softBound(Math.abs(y) / ry, pull, push)
  }
}

function smoothBound (pos, {rx, ry}, {squareness, roundness, ...settings}) {
  if (rx > ry) {
    rx = squareness * ry + (1 - squareness) * rx
  } else {
    ry = squareness * rx + (1 - squareness) * ry
  }
  let ovalBounded = ovalBound(pos, {rx, ry}, settings)
  let rectBounded = rectBound(pos, {rx, ry}, settings)
  return {
    x: roundness * ovalBounded.x + (1 - roundness) * rectBounded.x,
    y: roundness * ovalBounded.y + (1 - roundness) * rectBounded.y
  }
}

function makeCamera (bound) {
  return (prevPos, {x, y}, z, {w, h}, {area, ...settings}) => {
    let delta = {
      x: x - prevPos.x,
      y: y - prevPos.y
    }
    if (delta.x === 0 && delta.y === 0) {
      return prevPos
    }
    let bounded = bound(delta, {rx: area * z * w / 2, ry: area * z * h / 2}, settings)
    return {
      x: prevPos.x + delta.x - bounded.x,
      y: prevPos.y + delta.y - bounded.y
    }
  }
}

export default {
  circleCam: makeCamera(circleBound),
  ovalCam: makeCamera(ovalBound),
  rectCam: makeCamera(rectBound),
  smoothCam: makeCamera(smoothBound)
}
