/*
sol saved lines schema:
[
  {
    label: string,
    version: string,
    startPosition: {x: float, y: float},
    lines: [
      {
        x1: float,
        y1: float,
        x2: float,
        y2: float,
        extended: int (0-3),
        flipped: int (0-1),
        leftLine: int,
        rightLine: int,
        id: int,
        type: int (0-2)
      },
      ... older lines
    ]
  },
  ...older tracks
]
*/
var amf = require('amf');
var _ = require('lodash')

var DynBuf = require('./dynbuf')

// this stuff should rly be in solConsts.js
var LINE_ATTRIBUTES = [
'x1',
'y1',
'x2',
'y2',
'extended',
'flipped',
'leftLine',
'rightLine',
'id',
'type'
];

var OFFSETS = {
  MAGIC: 0,
  FILE_SIZE: 2,
  TAG: 6,
  MARKER: 10,
  SOL_NAME_LENGTH: 16,
  SOL_NAME: 18
};

var SOL_NAME_OFFSETS = {
  PADDING: 0,
  DATA_NAME_LENGTH: 4,
  DATA_NAME: 6
};

var HEADER = {
  MAGIC: 0x00bf,
  MARKER_TAG: 'TCSO',
  MARKER: 0x000400000000,
  PADDING: 0x00000000,
  SOL_NAME: 'savedLines',
  DATA_NAME: 'trackList'
};

function makeSol(writeTrackData) {
  let buf = new DynBuf()

  buf.writeUInt16BE(HEADER.MAGIC, OFFSETS.MAGIC)

  buf.write(HEADER.MARKER_TAG, OFFSETS.TAG, Buffer.byteLength(HEADER.MARKER_TAG, 'utf8'))

  buf.writeUIntBE(HEADER.MARKER, OFFSETS.MARKER, OFFSETS.SOL_NAME_LENGTH - OFFSETS.MARKER)

  let nameLength = Buffer.byteLength(HEADER.SOL_NAME, 'utf8')
  buf.writeUInt16BE(nameLength, OFFSETS.SOL_NAME_LENGTH)
  buf.write(HEADER.SOL_NAME, OFFSETS.SOL_NAME, nameLength)

  let paddingOffset = OFFSETS.SOL_NAME + nameLength + SOL_NAME_OFFSETS.PADDING;
  buf.writeUInt32BE(HEADER.PADDING, paddingOffset)

  let dataNameLengthOffset = OFFSETS.SOL_NAME + nameLength + SOL_NAME_OFFSETS.DATA_NAME_LENGTH
  let dataNameOffset = OFFSETS.SOL_NAME + nameLength + SOL_NAME_OFFSETS.DATA_NAME
  let dataNameLength = Buffer.byteLength(HEADER.DATA_NAME, 'utf8')
  let tracksDataOffset = dataNameOffset + dataNameLength
  buf.writeUInt16BE(dataNameLength, dataNameLengthOffset)
  buf.write(HEADER.DATA_NAME, dataNameOffset, dataNameLength)

  writeTrackData(buf, tracksDataOffset)

  buf.writeUInt32BE(buf.length - 5/* dont include padding at header and footer */, OFFSETS.FILE_SIZE)

  return buf.getBuffer()
}

module.exports = function solWriter(trackData) {

  let lines = _.sortBy(trackData.lines.slice(), 'id').reverse()

  let data = [{
    label: trackData.label,
    version: trackData.version,
    startLine: [trackData.startPosition.x, trackData.startPosition.y],
    data: lines.map(line => {
      return LINE_ATTRIBUTES.map(attribute => line[attribute])
    }),
    level: 1 + lines.reduce((max, {id}) => Math.max(max, id), 0)
  }]

  return makeSol((buf, tracksDataOffset) => {
    amf.write(buf, data, tracksDataOffset)
  })
}
