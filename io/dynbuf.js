/**
 * Dynamic Buffer, partial implementation
 * wraps around Buffer for write operations
 * use buf.getBuffer() to write and get the buffer
 */

// buf.write(string[, offset][, length][, encoding])
// UIntBE: 8 (value, offset, byteLength[, noAssert])
// UIntLE: 8 (value, offset, byteLength[, noAssert])
// IntBE: 8 (value, offset, byteLength[, noAssert])
// IntLE: 8 (value, offset, byteLength[, noAssert])
const writeFns = {
  write: null,
  writeUIntBE: null,
  writeUIntLE: null,
  writeIntBE: null,
  writeIntLE: null,
  writeDoubleBE: 64 / 8,
  writeDoubleLE: 64 / 8,
  writeFloatBE: 32 / 8,
  writeFloatLE: 32 / 8,
  writeInt8: 8 / 8,
  writeInt16BE: 16 / 8,
  writeInt16LE: 16 / 8,
  writeInt32BE: 32 / 8,
  writeInt32LE: 32 / 8,
  writeUInt8: 8 / 8,
  writeUInt16BE: 16 / 8,
  writeUInt16LE: 16 / 8,
  writeUInt32BE: 32 / 8,
  writeUInt32LE: 32 / 8
}

function DynBuf () {
  this.writeOps = []
  this.length = 0
}

Object.keys(writeFns).map(writeFn => {
  DynBuf.prototype[writeFn] = function (value, offset, ...opts) {
    this.writeOps.push([writeFn, value, offset, ...opts])
    let size = writeFns[writeFn]
    if (size == null) {
      size = opts[0]
    }
    this.length = Math.max(this.length, offset + size)
    // console.log(this.length, writeFn, value, offset, size)
    return size
  }
})

DynBuf.prototype.getBuffer = function () {
  let buf = new Buffer(this.length + 1)
  buf.writeUInt8(0, this.length) // add null byte at the end
  // console.log(buf)
  // console.log(this.length)
  // console.log(this.writeOps)
  this.writeOps.forEach(([writeFn, ...args]) => {
    // console.log(writeFn, args)
    buf[writeFn](...args)
  })
  return buf
}

// let b = new DynBuf()
// console.log(b.getBuffer())
// b.write('123', 0, 3)
// console.log(b.getBuffer())
// b.writeUInt32BE(0xdeadbeef, 3)
// console.log(b.getBuffer())

module.exports = DynBuf
