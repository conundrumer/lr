const MAGIC = 0xF24B5254
const VERSION = 1
// line types
const SCENERY = 0
const BLUE = 1
const RED = 2
const ClassicLineType = {
  [BLUE]: 0,
  [RED]: 1,
  [SCENERY]: 2
}
// features
const FEATURE_REDMULTIPLIER = 'REDMULTIPLIER'
const FEATURE_SCENERYWIDTH = 'SCENERYWIDTH'
const FEATURE_SONGINFO = 'SONGINFO'
const FEATURE_IGNORABLE_TRIGGER = 'IGNORABLE_TRIGGER'
const FEATURE_6_1 = '6.1'

const FeatureWarnings = {
  [FEATURE_REDMULTIPLIER]: 'Custom acceleration not supported!',
  [FEATURE_SCENERYWIDTH]: 'Custom scenery width not supported!',
  [FEATURE_SONGINFO]: 'Song information not supported!',
  [FEATURE_IGNORABLE_TRIGGER]: 'Line collision zooming not supported!',
  [FEATURE_6_1]: 'Using 6.1!'
}

// trk strings are inconsistent
// function parseTrkString (i, data) {
//   let length = data.readUInt16LE(i)
//   let string = data.toString('ascii', i + 2, i + 2 + length)
//   return [i + 2 + length, string]
// }

function parseTrkPoint (i, data) {
  return [i + 8 + 8, {x: data.readDoubleLE(i), y: data.readDoubleLE(i + 8)}]
}

function runParsers (initOffset, parsers) {
  return parsers.reduce((offset, parser) => parser(offset) || offset, initOffset)
}

function runParserMultipleTimes (initOffset, iterations, parser) {
  let offset = initOffset
  for (let i = 0; i < iterations; i++) {
    offset = parser(offset) || offset
  }
  return offset
}

function trkReader (data) {
  let features = {}
  let parsed = {};
  runParsers(0, [i => {
    let magic = data.readUInt32LE(i)
    if (magic !== MAGIC) {
      throw new Error(`Invalid magic number. Expected ${magic} to be ${MAGIC}`)
    }
    return i + 4
  }, i => {
    let version = data.readUInt8(i)
    if (version > VERSION) {
      throw new Error(`Only TRK versions ${VERSION} and below are supported. This version is ${version}`)
    }
    return i + 1
  }, i => {
    let length = data.readUInt16LE(i)
    let string = data.toString('ascii', i + 2, i + 2 + length)
    let [i_next, featureString] = [i + 2 + length, string]
    features = {}
    featureString.split(';').forEach(feature => {
      if (feature === '') return
      features[feature] = true
      console.warn(feature, FeatureWarnings[feature])
    })
    return i_next
  }, i => {
    if (features[FEATURE_SONGINFO]) {
      let length = data.readUInt8(i)
      let string = data.toString('ascii', i + 1, i + 1 + length)
      let [i_next, songInfo] = [i + 1 + length, string]
      parsed.songInfo = songInfo
      return i_next
    }
  }, i => {
    let [i_next, startPosition] = parseTrkPoint(i, data)
    parsed.startPosition = startPosition
    return i_next
  }, i => {
    let lineCount = data.readUInt32LE(i)
    let solidLines = []
    let sceneryLines = []
    runParserMultipleTimes(i + 4, lineCount, j => {
      let line = {}

      let j_next = runParsers(j, [k => {
        let typeflags = data.readUInt8(j)
        line.type = typeflags & 0x1F
        if (line.type !== SCENERY) {
          line.inv = (typeflags >> 7) !== 0
          line.lim = (typeflags >> 5) & 0x3
        }
        return k + 1
      }, k => {
        if (features[FEATURE_REDMULTIPLIER] && line.type === RED) {
          line.multiplier = data.readUInt8(k)
          return k + 1
        }
      }, k => {
        if (features[FEATURE_IGNORABLE_TRIGGER] && (line.type === BLUE || line.type === RED)) {
          let haszoomtrigger = data.readUInt8(k)
          if (haszoomtrigger) {
            return runParsers(k + 1, [l => {
              line.zoomtarget = data.readFloatLE(l)
              return l + 4
            }, l => {
              line.zoomframes = data.readUInt16LE(l)
              return l + 2
            }])
          }
          return k + 1
        }
      }, k => {
        if (line.type === BLUE || line.type === RED) {
          line.id = data.readUInt32LE(k)
          return k + 4
        }
      }, k => {
        if (line.lim != null && line.lim !== 0) {
          line.prev = data.readInt32LE(k)
          line.next = data.readInt32LE(k + 4)
          return k + 8
        }
      }, k => {
        if (features[FEATURE_SCENERYWIDTH] && line.type === SCENERY) {
          line.width = data.readUInt8(k) / 10
          return k + 1
        }
      }, k => {
        let [k_next, p1] = parseTrkPoint(k, data)
        line.x1 = p1.x
        line.y1 = p1.y
        return k_next
      }, k => {
        let [k_next, p2] = parseTrkPoint(k, data)
        line.x2 = p2.x
        line.y2 = p2.y
        return k_next
      }])

      switch (line.type) {
        case BLUE:
        case RED:
          solidLines.push(line)
          break
        case SCENERY:
          sceneryLines.push(line)
          break
      }
      return j_next
    })
    let nextID = solidLines.map(({id}) => id).reduce((id1, id2) => Math.max(id1, id2), 0)
    sceneryLines.forEach(line => {
      nextID += 1
      line.id = nextID
    })
    parsed.lines = solidLines.concat(sceneryLines)
  }])

  return {
    label: 'lra track',
    version: features[FEATURE_6_1] ? '6.1' : '6.2',
    startPosition: parsed.startPosition,
    lines: parsed.lines.map(line => {
      let {x1, y1, x2, y2,
        lim, inv, prev, next,
        id, type
      } = line
      return {x1, y1, x2, y2,
        extended: lim,
        flipped: inv,
        leftLine: prev,
        rightLine: next,
        id, type: ClassicLineType[type]
      }
    })
  }
}

module.exports = trkReader

// print to console

// var SAVEDLINES = 'inf.trk';
// var fs = require('fs');

// fs.readFile(SAVEDLINES, (err, data) => {
//   if (err) {
//     return console.error(err);
//   }
//   var track = trkReader(data);
//   var lines = track.lines;
//   delete track.lines;
//   console.log(track);
//   console.log('Line count:', lines.length);
//   console.log('--- Lines ---');
//   // console.log(lines);
// });

/*
little endian
strings are prefixed with an int16 denoting length, ASCII
point64 is shorthand for two doubles, x and y.
enum LineType {
  Scenery=0,
  Blue=1,
  Red = 2,
}
CURRENT FEATURE STRINGS:REDMULTIPLIER;SCENERYWIDTH;6.1;SONGINFO;IGNORABLE_TRIGGER;
MAGIC = TRK\xF2, as a hex int: 0xF24B5254

[int32:MAGIC]
[byte:version] // 1
[string:features]//caps, seperated by semicolons. "REDMULTIPLIER;SONGINFO;SCENERYWIDTH" just for example. Always check every feature is supported before continuing to load. provided for future and backwards compatibility.
[point64:start point]
if (feature_6.1) {
  //set physics to 6.1 here
}
if (feature_songinfo) {
  [string:song_info]//split by \r\n, should only be two strings as a result. one is the name, one is a float32 denoting start offset. data will be something like example.mp3\r\n10.5
}
[int32: line count] {
  [int8:typeflags]=
    linetype = (typeflags & 0x1F),
    line_inv = (typeflags >> 7) != 0,//only matters if not scenery
    line_limit = (typeflags >> 5) & 0x3;//only matters if not scenery

  if (linetype == red) {
    if (feature_redmultiplier) {
      [int8:multiplier]
    }
  }
  if (linetype == blue || linetype == red) {
    if (feature_ignorabletrigger) {
      if ([bool:haszoomtrigger]) {
        [float32:zoomtarget]
        [int16:zoomframes]
      }
    }
    [int32:line ID]
    if (lim != 0) {
      [int32:prev line ID]//if no prev line, -1
      [int32:next line ID]//if no next line, -1
    }
  } else if (linetype == scenery) {
    if (feature_scenerywidth) {
      [int8:(width*10)]// divide by 10 to get linewidth. didn't want to store a whole float
    }
  }
  [point64:line point 1]
  [point64:line point 2]
}
[EOF]
*/
