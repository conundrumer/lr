var Millions = require('millions');

// these functions should yield the endpoints of Millions lines as an array.
// the caller will then add these lines into a scene and track the relevant
// handles, so that when we need to generate these lines after undo/redo we can
// assign them the appropriate handles.
//
// these functions *MUST* be pure - that is for a given line they should *ALWAYS*
// yield the same results.
//
// the yielded lines are added to the scene in yield order, so yield from bottom
// to top.

const LINE_WIDTH = 2;

export function* genEditorLinesForSolidLine(line) {
    var data = line.getData();

    // line color
    let offsetAmount = LINE_WIDTH / 2;
    if (data.flipped) {
        offsetAmount *= -1;
    }

    let offset = line.norm.clone().mulS(offsetAmount);
    let c1 = line.p.clone().add(offset);
    let c2 = line.q.clone().add(offset);

    yield new Millions.Line({
        x: c1.x,
        y: c1.y,
        color: Millions.Color.fromRGB(33, 150, 243),
        thickness: LINE_WIDTH
    }, {
        x: c2.x,
        y: c2.y,
        color: Millions.Color.fromRGB(33, 150, 243),
        thickness: LINE_WIDTH
    }, line.id);

    // line floor
    yield new Millions.Line({
        x: data.x1,
        y: data.y1,
        color: Millions.Color.fromRGB(0, 0, 0),
        thickness: LINE_WIDTH
    }, {
        x: data.x2,
        y: data.y2,
        color: Millions.Color.fromRGB(0, 0, 0),
        thickness: LINE_WIDTH
    }, line.id + 0.1);
}

export function* genEditorLinesForAccLine(line) {
    var data = line.getData();

    // line color
    let offsetAmount = LINE_WIDTH / 2;

    let offset = line.norm.clone().mulS(offsetAmount);
    let c1 = line.p.clone().add(offset);
    let c2 = line.q.clone().add(offset);

    let dx = c2.x - c1.x,
        dy = c2.y - c1.y;

    let len = Math.sqrt(dx*dx + dy*dy);
    let ux = dx / len,
        uy = dy / len;

    let end = c2;

    yield new Millions.Line({
        x: c1.x,
        y: c1.y,
        color: Millions.Color.fromRGB(244, 67, 54),
        thickness: LINE_WIDTH
    }, {
        x: c2.x,
        y: c2.y,
        color: Millions.Color.fromRGB(244, 67, 54),
        thickness: LINE_WIDTH
    }, line.id);

    yield new Millions.Triangle({
        x: end.x,
        y: end.y,
        color: Millions.Color.fromRGB(244, 67, 54)
    }, {
        x: end.x - ux * 5,
        y: end.y - uy * 5,
        color: Millions.Color.fromRGB(244, 67, 54)
    }, {
        x: end.x - ux * 5 + offset.x * 4,
        y: end.y - uy * 5 + offset.y * 4,
        color: Millions.Color.fromRGB(244, 67, 54)
    }, line.id + 0.1);

    // line floor
    yield new Millions.Line({
        x: data.x1,
        y: data.y1,
        color: Millions.Color.fromRGB(0, 0, 0),
        thickness: LINE_WIDTH
    }, {
        x: data.x2,
        y: data.y2,
        color: Millions.Color.fromRGB(0, 0, 0),
        thickness: LINE_WIDTH
    }, line.id + 0.2);
}

export function* genEditorLinesForSceneryLine(line) {
    var data = line.getData();

    // line floor/color
    yield new Millions.Line({
        x: data.x1,
        y: data.y1,
        color: Millions.Color.fromRGB(76, 175, 80),
        thickness: LINE_WIDTH
    }, {
        x: data.x2,
        y: data.y2,
        color: Millions.Color.fromRGB(76, 175, 80),
        thickness: LINE_WIDTH
    }, line.id);
}

export function* genPlaybackLinesForLine(line) {
    var data = line.getData();

    // line floor/color
    yield new Millions.Line({
        x: data.x1,
        y: data.y1,
        color: Millions.Color.fromRGB(0, 0, 0),
        thickness: LINE_WIDTH
    }, {
        x: data.x2,
        y: data.y2,
        color: Millions.Color.fromRGB(0, 0, 0),
        thickness: LINE_WIDTH
    }, line.id);
}
