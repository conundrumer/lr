'use strict';

var React = require('react');
import ReactDOM from 'react-dom';
var _ = require('lodash');

var Millions = require('millions');
var MillionsReact = require('millions-react');
var lineGen = require('./millionsLineGen');
var Immy = require('immy');

const LINE_WIDTH = 2;
var { SOLID_LINE, ACC_LINE, SCENERY_LINE } = require('../core').LineTypes;

export default class MillionsLineDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editScene: new Millions.Scene(),
            playbackScene: new Millions.Scene()
        };
    }

    getImageData() {
        return null;
    }

    getResolution () {
        return this.props.resolution || window.devicePixelRatio || 1;
    }

    componentWillReceiveProps(newProps) {
        var diff = this.props.lines.compareTo(newProps.lines);
        var editScene = this.state.editScene;
        var playbackScene = this.state.playbackScene;

        diff.forEachPrimitive((primOp) => {
            if (primOp instanceof Immy.ListPatches.Add) {
                let generator = null;

                switch (primOp.value.type) {
                case SOLID_LINE:
                    generator = lineGen.genEditorLinesForSolidLine;
                    break;

                case ACC_LINE:
                    generator = lineGen.genEditorLinesForAccLine;
                    break;

                case SCENERY_LINE:
                    generator = lineGen.genEditorLinesForSceneryLine;
                    break;

                default:
                    throw new Error('unknown line type');
                }

                for (let entity of generator(primOp.value)) {
                    editScene = editScene.withEntityAdded(entity);
                }

                for (let entity of lineGen.genPlaybackLinesForLine(primOp.value)) {
                    playbackScene = playbackScene.withEntityAdded(entity);
                }
            } else {
                // must be a remove, nuke the line's entities from the scene
                editScene = editScene.withEntitiesInZIndexRangeRemoved(primOp.value.id, primOp.value.id + 1);
                playbackScene = playbackScene.withEntitiesInZIndexRangeRemoved(primOp.value.id, primOp.value.id + 1);
            }
        });

        this.setState({
            editScene: editScene,
            playbackScene: playbackScene
        });
    }

    render() {
        let r = this.getResolution();
        let { width, height, cam } = this.props;

        let camera = new Millions.Camera()
            .withAspectRatio(width / height)
            .withFocalPoint(cam.x, cam.y)
            .withZoom(1 / cam.z);

        let scene = this.state.playbackScene;
        if (this.props.color) {
            scene = this.state.editScene;
        }

        return (
            <MillionsReact.Renderer style={{position: 'absolute', width: width, height: height}}
                scene={scene} camera={camera} />
        );
  }
}

var millionsSupported = null;
MillionsLineDisplay.isSupported = function () {
    if (window.forceRenderer == 'millions') {
        return true;
    } else if (window.forceRenderer == 'canvas') {
        return false;
    }

    if (millionsSupported === null) {
        try {
            var renderer = Millions.getBestSupportedRenderer();
            millionsSupported = true;
        } catch (e) {
            millionsSupported = false;
        }
    }

    return millionsSupported;
};
