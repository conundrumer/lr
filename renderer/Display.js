'use strict';

var React = require('react');
import ReactDOM from 'react-dom'

var Rider = require('./Rider');
import Flag from './Flag'
import LineSelection from './SvgLineSelection'
// var Grid = require('./Grid'); // TODO: make separate debug display
// var Lines = require('./SvgLineDisplay');
var CanvasLineDisplay = require('./CanvasLineDisplay');
// var Lines = require('./PixiLineDisplay');
var MillionsLineDisplay = require('./MillionsLineDisplay');

const PRECISION = 1000;

function round(x) {
  return ((x * PRECISION + 0.5) | 0) / PRECISION;
}

const CameraMask = ({viewBox, dimensions: {width, height}, cam: {x, y, z}}) => (
  <svg style={{position: 'absolute'}} viewBox={viewBox}>
    <path d={[
      'M', x - width * z, y - height * z,
      'H', x + width * z,
      'V', y + height * z,
      'H', x - width * z,
      'z',
      'M', x - (width / 2) * z, y - (height / 2) * z,
      'V', y + (height / 2) * z,
      'H', x + (width / 2) * z,
      'V', y - (height / 2) * z,
      'z'
    ].join(' ')} fill='rgba(0,0,0,0.5)' />
  </svg>
)

window.speed = true
var serializer = new XMLSerializer()
export default class Display extends React.Component {

  shouldComponentUpdate(nextProps) {
    return !this.props.display || this.props.display !== nextProps.display;
  }

  renderToCanvas (canvas, img, cb) {
    let {width, height} = this.props
    let ctx = canvas.getContext('2d')
    if (!window.hideLines) {
      ctx.putImageData(this.refs.lines.getImageData(), 0, 0)
    } else {
      ctx.fillStyle = 'white'
      ctx.fillRect(0, 0, width, height)
    }

    let riderSvg = ReactDOM.findDOMNode(this.refs.rider)
    riderSvg = riderSvg.cloneNode(true)
    riderSvg.setAttribute('width', width)
    riderSvg.setAttribute('height', height)

    let svgData = serializer.serializeToString(riderSvg)
    img.setAttribute('src', 'data:image/svg+xml;base64,' + btoa(svgData))

    img.onload = function() {
      ctx.drawImage( img, 0, 0, width, height)
      cb()
    }
  }

  getViewBox() {
    let {cam: {x, y, z}, width: w, height: h} = this.props;
    return [
      round(x - w / 2 * z),
      round(y - h / 2 * z),
      round(w * z),
      round(h * z)
    ];
  }

  getStyle() {
    return {
      position: 'absolute',
      width: this.props.width,
      height: this.props.height
    };
  }

  renderLines() {
    if (window.hideLines) return <div></div>
    if (MillionsLineDisplay.isSupported() && !this.props.useCanvas) {
      return <MillionsLineDisplay ref='lines' {...this.props} {...this.props.viewOptions} viewBox={this.getViewBox()} />
    } else {
      let w = this.props.width,
          h = this.props.height,
          { x, y, z } = this.props.cam;

      // the canvas line display needs to only be given visible lines, so we do
      // that selection here (*not* as a selector because then you're always
      // selecting and ruining the Millions performance)
      let [x1, y1, width, height] = [
        x - w / 2 * z,
        y - h / 2 * z,
        w * z,
        h * z
      ];

      // a way to pass down all props except for lines, which we override with
      // the selected ones
      let { lines, ...others } = this.props;
      lines = this.props.track.getLinesInBox(x1, y1, x1 + width, y1 + height);

      return <CanvasLineDisplay ref='lines' lines={lines} {...others} {...this.props.viewOptions} viewBox={this.getViewBox()} />
    }
  }

  render() {
    let viewOptions = this.props.viewOptions;
    let viewBox = this.getViewBox();
    let {x, y} = this.props.startPosition;
    let seed = x * x + y * y;

    let viewport = {
      ...this.props.cam,
      w: this.props.width,
      h: this.props.height
    }

    let dimensions = window.cam.dimensions

    return (
      <div ref='container' style={this.getStyle()} >
        { this.renderLines() }
        <svg ref='rider' style={{position: 'absolute'}} viewBox={viewBox}>
          {this.props.hideFlags ? null :
            <g>
              <Flag icon={this.props.startIcon} pos={{x, y: y + 5}} zoom={this.props.cam.z} />
              <Flag icon={this.props.flagIcon} pos={this.props.flagRider.points[1].pos} zoom={this.props.cam.z} />
            </g>
          }
          <Rider
            i={0}
            showContactPoints={viewOptions.showContactPoints}
            rider={this.props.rider}
            riders={this.props.riders}
            index={this.props.frame}
            flagIndex={this.props.flagIndex}
            startIndex={this.props.startIndex}
            endIndex={this.props.endIndex}
            seed={seed}
            onionSkin={this.props.onionSkin}
          />
        </svg>
        <LineSelection lines={this.props.lineSelection} viewport={viewport} />
        {dimensions && <CameraMask dimensions={dimensions} viewBox={viewBox} cam={this.props.playbackCam} />}
      </div>
    );
  }

}

var PropTypes = React.PropTypes;
Display.propTypes = {
  display: PropTypes.object,
  frame: PropTypes.number.isRequired,
  flagIndex: PropTypes.number.isRequired,
  maxIndex: PropTypes.number.isRequired,
  startIndex: PropTypes.number.isRequired,
  endIndex: PropTypes.number.isRequired,
  lines: PropTypes.object.isRequired,
  rider: PropTypes.object.isRequired,
  flagRider: PropTypes.object.isRequired,
  riders: PropTypes.arrayOf(PropTypes.object).isRequired,
  onionSkin: PropTypes.bool.isRequired,
  startPosition: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired
  }).isRequired,
  cam: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    z: PropTypes.number.isRequired
  }).isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  viewOptions: PropTypes.shape({
    color: PropTypes.bool,
    floor: PropTypes.bool,
    accArrow: PropTypes.bool,
    snapDot: PropTypes.bool
  }),
  startIcon: PropTypes.element,
  flagIcon: PropTypes.element,
  endIcon: PropTypes.element,
  hideFlags: PropTypes.bool,
  resolution: PropTypes.number,
  lineSelection: PropTypes.arrayOf(PropTypes.object).isRequired
}
