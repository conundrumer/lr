var webpack = require('webpack');

var config = require('./webpack.config.dev');

module.exports = Object.assign(config, {
  devtool: 'source-map',
  entry: ['babel-polyfill', './editor/main'],
  plugins: [
    new webpack.DefinePlugin({
      __DEVTOOLS__: false,
      'process.env.NODE_ENV': '"production"'
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.AggressiveMergingPlugin()
  ]
});
