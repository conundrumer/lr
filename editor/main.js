'use strict';
var injectTapEventPlugin = require("react-tap-event-plugin");
//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();
require('normalize.css');

require('assets/fonts.css');
require('./styles/main.less');

var React = require('react');
import ReactDOM from 'react-dom'
var { createStore, applyMiddleware, combineReducers } = require('redux');
var { Provider } = require('react-redux');
var thunk = require('redux-thunk');

let {track: initTrack, audio: audioURL, offset, fps, night} = queryString.parse(location.search)
window.night = night

var reducers = require('./reducers');
var App = require('./components/App');

let render = (store, rootElement) => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    rootElement
  );
}
import {NEW_TRACK, LOAD_TRACK, ADD_LINE, REMOVE_LINE, REPLACE_LINE} from './actions'

import { trackCache } from './trackCacheMiddleware'
import audiosync from './audiosyncMiddleware'

let confirmNewTrack = ({getState}) => next => action => {
  if (action.type === NEW_TRACK) {
    // there's a better way to do this but this will do for now
    // http://alistapart.com/article/neveruseawarning
    if (window.confirm('Are you sure you want to make a new track?')) {
      return next(action)
    }
  } else {
    return next(action)
  }
}

let middlewares = [thunk, confirmNewTrack, trackCache(), audiosync]

if (process.env.NODE_ENV !== 'production') {
  middlewares.push(require('redux-logger')({
    collapsed: true,
    predicate: (getState, action) => action.type !== 'SET_MOD_KEY'
  }))
}

let enhanceStore = applyMiddleware(...middlewares)
if (__DEVTOOLS__) { // eslint-disable-line no-undef

  let { devTools, persistState } = require('redux-devtools')
  let { DevTools, DebugPanel, LogMonitor } = require('redux-devtools/lib/react')
  let { compose } = require('redux')
  let Immutable = require('immutable')


  let makeLineStore = lineStore => Immutable.Map(lineStore).mapKeys(key => parseInt(key, 10))

  enhanceStore = compose(
    enhanceStore,
    devTools(),
    persistState(
      window.location.href.match(/[?&]debug_session=([^&]+)\b/),
      state => state && ({...state,
        trackData: {...state.trackData,
          lineStore: makeLineStore(state.trackData.lineStore)
        },
        history: {
          undoStack: Immutable.Stack(state.history.undoStack),
          redoStack: Immutable.Stack(state.history.redoStack)
        }
      }),
      action => {
        switch (action.type) {
          case NEW_TRACK:
          case LOAD_TRACK:
          case ADD_LINE:
          case REMOVE_LINE:
          case REPLACE_LINE:
            return {...action,
              lineStore: makeLineStore(action.lineStore)
            }
          default:
            return action
        }
      }
    )
  )

  render = (store, rootElement) => {
    ReactDOM.render(
      <div>
        <Provider store={store}>
          <App />
        </Provider>
        <DebugPanel top right bottom>
          <DevTools store={store} monitor={LogMonitor} />
        </DebugPanel>
      </div>,
      rootElement
    );
  }

}

const reducer = combineReducers(reducers);
const createAppStore = enhanceStore(createStore)

let store = createAppStore(reducer)

window.onbeforeunload = () => 'You are about to exit. I should check if you saved your track.'

window.store = store
/* temporary advanced settings for color playback */
window.colorLock = undefined
/* temporary advanced settings for camera lock */
window.camLock = undefined
/* temporary console auto-zoom */
window.getAutoZoom = undefined

/* temporary console UI hiding */

window.toggleUI = function () {
  let displayed = document.querySelector('.LR-Editor').style.display
  document.querySelector('.LR-Editor').style.display = displayed ? null : 'none'
}
/* /temporary console UI hiding */

/* temporary advanced settings for camera following */
window.cam = {
  area: 0.4,
  pull: 0.01,
  push: 0.8,
  roundness: 0.5,
  squareness: 0,
  dimensions: undefined
}

/* temporary settings for recording dimensions */
window.recorder = {
  width: 1280,
  height: 720
}

/* console-based auto-zoomer */
import createZoomer from './zoomer.js'
window.createZoomer = createZoomer

render(store, document.getElementById('content'))

import {getTrackFromURL, getAudioFromURL, setAudioOffset} from './actions'
import queryString from 'query-string'

if (initTrack) {
  initTrack = initTrack.replace('https://www.dropbox.com', 'https://dl.dropboxusercontent.com')
  store.dispatch(getTrackFromURL(initTrack))
}

if (audioURL) {
  audioURL = audioURL.replace('https://www.dropbox.com', 'https://dl.dropboxusercontent.com')
  store.dispatch(getAudioFromURL(audioURL))
  if (offset) {
    store.dispatch(setAudioOffset(parseFloat(offset)))
  }
}

fps = parseInt(fps, 10)
window.FPS = (fps > 0) ? fps : 40

