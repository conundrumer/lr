'use strict';

import Immutable from 'immutable';

import {
  RESIZE,
  SET_MOD_KEY,
  SHOW_TOOLBARS,
  HIDE_TOOLBARS,
  SHOW_SIDEBAR,
  HIDE_SIDEBAR,
  SELECT_SIDEBAR_ITEM,
  SHOW_FILE_LOADER,
  HIDE_FILE_LOADER,
  TOGGLE_TIME_CONTROL,
  TOGGLE_BUTTON,
  SET_TOOL,
  SET_HOTKEY,
  SET_CAM,
  SET_FRAME_INDEX,
  SET_FRAME_MAX_INDEX,
  SET_FRAME_RATE,
  INC_FRAME_INDEX,
  DEC_FRAME_INDEX,
  SET_PLAYBACK_STATE,
  MOD_PLAYBACK_STATE,
  ADD_LINE,
  REMOVE_LINE,
  REPLACE_LINE,
  NEW_TRACK,
  LOAD_TRACK,
  LOAD_FILE,
  LOAD_FILE_SUCCESS,
  LOAD_FILE_FAIL,
  IMPORT_TRACK,
  CANCEL_IMPORT,
  SET_FLAG,
  SELECT_COLOR,
  SHOW_TRACK_SAVER,
  HIDE_TRACK_SAVER,
  SET_TRACK_NAME,
  PUSH_ACTION,
  UNDO,
  REDO,
  DRAW_STREAM_START,
  DRAW_STREAM_END,
  SELECT_LINE,
  UNSELECT_LINE,
  GET_RELEASE_INFO,
  START_RECORDING,
  STOP_RECORDING,
  LOAD_AUDIO,
  SET_AUDIO_OFFSET,
  TOGGLE_AUDIO,
  SHOW_AUDIO_FILE_LOADER,
  HIDE_AUDIO_FILE_LOADER,
  LOAD_AUDIO_PENDING,
  LOAD_AUDIO_FAIL
} from './actions';

import { smoothCam } from 'engine/camera'
import { newTrack } from './actions';
// TODO: combine cam and windowSize to viewport
const INIT = {
  viewport: {
    w: 1,
    h: 1,
    x: 0,
    y: 0,
    z: 0.5,
    initCam: {x: 0, y: 0}, // i need to organize my reducers better
    cam: {x: 0, y: 0}
  },
  toolbars: {
    toolbarsOpen: false,
    timeControlOpen: false,
    sidebarOpen: false,
    sidebarSelected: -1,
    colorSelected: 0
  },
  modKeys: {
    shift: false,
    mod: false,
    alt: false
  },
  selectedTool: 'pencil',
  toggled: {},
  hotkeys: {},
  playback: {
    state: 'stop',
    modState: null,
    index: 0,
    maxIndex: 0,
    rate: 0,
    skipFrames: false,
    flag: 0
  },
  fileLoader: {
    open: false,
    loadingFile: false,
    error: null,
    fileName: null,
    tracks: null
  },
  trackSaver: {
    open: false
  },
  trackData: trackData(null, newTrack()),
  history: {
    undoStack: Immutable.Stack(),
    redoStack: Immutable.Stack()
  },
  drawStreamActive: false, // cancellable draw streams
  lineSelection: {
    lineID: null
  },
  releaseInfo: [],
  recording: false,
  loadedTrack: false,
  audio: {
    enabled: false,
    url: null,
    offset: 0
  },
  audioFileLoader: { // omg wtf lol not dry bruh ;_;
    open: false,
    loadingFile: false,
    error: null
  }
};

export function audioFileLoader (state = INIT.audioFileLoader, action) {
  switch (action.type) {
    case SHOW_AUDIO_FILE_LOADER:
      return {
        ...state,
        open: true
      }
    case HIDE_AUDIO_FILE_LOADER:
      return {...state,
        open: false,
        error: null
      }
    case LOAD_AUDIO_PENDING:
      return {...state,
        loadingFile: true
      }
    case LOAD_AUDIO:
      return {...state,
        open: false,
        loadingFile: false,
        error: null
      }
    case LOAD_AUDIO_FAIL:
      return {...state,
        loadingFile: false,
        error: action.error
      }
    default:
      return state
  }
}

export function audio (state = INIT.audio, action) {
  switch (action.type) {
    // case NEW_TRACK:
    // case LOAD_TRACK:
    //   return INIT.audio
    case LOAD_AUDIO:
      return {
        enabled: action.enabled,
        url: action.enabled ? action.url : null,
        offset: INIT.audio.offset
      }
    case SET_AUDIO_OFFSET:
      return {...state,
        offset: action.offset
      }
    case TOGGLE_AUDIO:
      return {...state,
        enabled: state.url ? !state.enabled : false
      }
    case START_RECORDING:
      return {...state,
        enabled: false
      }
    default:
      return state
  }
}

export function loadedTrack (state = INIT.loadedTrack, action) {
  switch (action.type) {
    case LOAD_TRACK:
      return true
    default:
      return state
  }
}

export function recording (state = INIT.recording, action) {
  switch (action.type) {
    case START_RECORDING:
      return true
    case STOP_RECORDING:
      return false
    default:
      return state
  }
}

export function releaseInfo(state = INIT.releaseInfo, action) {
  switch (action.type) {
    case GET_RELEASE_INFO:
      return action.body
    default:
      return state
  }
}

export function lineSelection(state = INIT.lineSelection, action) {
  switch (action.type) {
    case SELECT_LINE:
      return {
        lineID: action.lineID // TODO: multiple selection
      }
    case SET_TOOL:
      return {
        lineID: action.tool === 'select' ? state.lineID : null
      }
    default:
      return state
  }
}

export function drawStreamActive(state = INIT.drawStreamActive, action) {
  switch (action.type) {
    case DRAW_STREAM_START:
      return true
    case DRAW_STREAM_END:
      return false
    default:
      return state
  }
}

// display dimensions
export function viewport(state = INIT.viewport, action) {
  switch (action.type) {
    case INC_FRAME_INDEX:
    case DEC_FRAME_INDEX:
    case SET_FRAME_INDEX:
      /* temporary console auto-zoom */
      let z = window.getAutoZoom && window.getAutoZoom(action.index) || state.z
      let dimensions = window.cam.dimensions
      /* /temporary console auto-zoom */
      let cam = state.cam
      if (dimensions) {
        dimensions = {
          w: dimensions.width,
          h: dimensions.height
        }
      } else {
        dimensions = state
      }
      if (action.index > 0) {
        if (action.riderPosition) {
          cam = smoothCam(cam, action.riderPosition, z, dimensions, window.cam)
        } else {
          for (let riderPosition of action.riderPositions) {
            cam = smoothCam(cam, riderPosition, z, dimensions, window.cam)
          }
        }
      } else {
        cam = state.initCam
      }
      return {...state,
        cam
      }
    case LOAD_TRACK:
      return {...state,
        x: action.startPosition.x,
        y: action.startPosition.y,
        z: INIT.viewport.z,
        initCam: action.startPosition,
        cam: action.startPosition
      }
    case RESIZE:
      return {...state,
        w: action.windowSize.width,
        h: action.windowSize.height
      };
    case SET_CAM:
      return {...state,
        x: action.cam.x,
        y: action.cam.y,
        z: action.cam.z
      }
    case NEW_TRACK:
      return {...state,
        initCam: INIT.viewport.initCam,
        cam: INIT.viewport.cam,
        x: INIT.viewport.x,
        y: INIT.viewport.y,
        z: INIT.viewport.z
      }
    default:
      return state;
  }
}

export function modKeys(state = INIT.modKeys, action) {
  switch (action.type) {
    case SET_MOD_KEY:
      return {...state,
        [action.key]: action.pressed
      }
    default:
      return state;
  }
}
// toolbars
export function toolbars(state = INIT.toolbars, action) {
  switch (action.type) {
    case SELECT_COLOR:
      return {...state,
        colorSelected: action.color
      }
    case SHOW_TOOLBARS:
      return {...state,
        toolbarsOpen: true
      };
    case HIDE_TOOLBARS:
      return {...state,
        toolbarsOpen: false
      };
    case LOAD_FILE_SUCCESS:
      return {...state,
        sidebarOpen: true,
        sidebarSelected: INIT.toolbars.sidebarSelected
      }
    case SHOW_SIDEBAR:
      return {...state,
        sidebarOpen: true
      };
    case IMPORT_TRACK:
    case CANCEL_IMPORT:
      /* TODO: distinguish sidebarOpen from other sidebar panels */
    case HIDE_SIDEBAR:
      return {...state,
        sidebarOpen: false
      };
    case SELECT_SIDEBAR_ITEM:
      return {...state,
        sidebarSelected: action.selected
      }
    case TOGGLE_TIME_CONTROL:
      return {...state,
        timeControlOpen: !state.timeControlOpen
      };
    default:
      return state;
  }
}

export function selectedTool(state = INIT.selectedTool, action) {
  switch (action.type) {
    case SET_TOOL:
      return action.tool
    default:
      return state;
  }
}

export function fileLoader(state = INIT.fileLoader, action) {
  switch (action.type) {
    case SHOW_FILE_LOADER:
      return {...state,
        open: true
      };
    case HIDE_FILE_LOADER:
      return {...state,
        open: false
      };
    case LOAD_FILE:
      return {...state,
        loadingFile: true
      }
    case LOAD_FILE_SUCCESS:
      return {...state,
        loadingFile: false,
        open: false,
        fileName: action.fileName,
        tracks: action.tracks
      }
    case LOAD_FILE_FAIL:
      return {...state,
        loadingFile: false,
        error: action.error
      }
    default:
      return state;
  }
}
export function trackSaver(state = INIT.trackSaver, action) {
  switch (action.type) {
    case SHOW_TRACK_SAVER:
      return {...state,
        open: true
      }
    case HIDE_TRACK_SAVER:
      return {...state,
        open: false
      }
    default:
      return state
  }
}

export function toggled(state = INIT.toggled, action) {
  switch (action.type) {
    case TOGGLE_BUTTON:
      let isToggled;
      if (typeof action.isToggled === 'boolean') {
        isToggled = action.isToggled;
      } else {
        isToggled = !state[action.name];
      }
      return {...state,
        [action.name]: isToggled
      };
    default:
      return state;
  }
}

export function hotkeys(state = INIT.hotkeys, action) {
  switch (action.type) {
    case SET_HOTKEY:
      return {...state,
        [action.name]: action.hotkey
      };
    default:
      return state;
  }
}

export function playback(state = INIT.playback, action) {
  switch (action.type) {
    case INC_FRAME_INDEX:
      action.index = state.index + 1;
      break;
    case DEC_FRAME_INDEX:
      action.index = state.index - 1;
      break;
  }
  switch (action.type) {
    case INC_FRAME_INDEX:
    case DEC_FRAME_INDEX:
    case SET_FRAME_INDEX:
      let index = Math.max(0, action.index);
      return {...state,
        index: index,
        maxIndex: Math.max(state.maxIndex, index)
      };
    case SET_FRAME_MAX_INDEX:
      let maxIndex = Math.max(0, action.maxIndex);
      return {...state,
        index: Math.min(state.index, maxIndex),
        maxIndex: maxIndex,
      };
    case SET_FRAME_RATE:
      return {...state,
        rate: action.rate,
      };
    case SET_PLAYBACK_STATE:
      return {...state,
        state: action.state
      };
    case MOD_PLAYBACK_STATE:
      return {...state,
        modState: action.mod
      };
    case SET_FLAG:
      if (action.index != null) {
        return {...state,
          flag: action.index
        }
      } else if (state.flag === state.index && state.state === 'stop') { // TODO: reset flag if pressed twice
        return {...state,
          flag: INIT.playback.flag,
          index: INIT.playback.index
        }
      }
      return {...state,
        flag: state.index
      }
    case NEW_TRACK:
    case LOAD_TRACK:
      return {...INIT.playback,
        maxIndex: action.maxIndex != null ? action.maxIndex : INIT.playback.maxIndex
      }
    default:
      return state;
  }
}

export function trackData(state = INIT.trackData, action) {
  switch (action.type) {
    case NEW_TRACK:
    case LOAD_TRACK:
      let { startPosition, version, label, lineStore } = action
      return {
        maxLineID: action.maxLineID || 0,
        saved: false,
        lineStore: lineStore,
        startPosition,
        version,
        label
      }
    case SET_TRACK_NAME:
      return {...state,
        label: action.name
      }
    case ADD_LINE:
    case REMOVE_LINE:
    case REPLACE_LINE:
      return {...state,
        maxLineID: action.maxLineID,
        lineStore: action.lineStore
      }
    default:
      return state;
  }
}

function getAction({action: {type, line, prevLine}}) {
  return {type, line, prevLine}
}
export function history(state = INIT.history, action) {
  switch (action.type) {
    case PUSH_ACTION:
      return {
        undoStack: state.undoStack.push(getAction(action)),
        redoStack: INIT.history.redoStack
      }
    case UNDO:
      if (state.undoStack.size === 0) {
        return state
      }
      return {
        redoStack: state.redoStack.push(state.undoStack.peek()),
        undoStack: state.undoStack.pop()
      }
    case REDO:
      if (state.redoStack.size === 0) {
        return state
      }
      return {
        undoStack: state.undoStack.push(state.redoStack.peek()),
        redoStack: state.redoStack.pop()
      }
    default:
      return state
  }
}
