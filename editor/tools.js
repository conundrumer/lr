'use strict';

import Vector from 'core/Vector'
import { setCam, addLine, removeLine, replaceLine, pushAction, selectLine } from './actions';
import { getTrackFromCache } from './trackCacheMiddleware'

export function debugTool(stream, dispatch, getState) {
  stream.first().subscribe(pos => {
    console.log('start', pos.x, pos.y);
  });
  return {
    stream: stream.skip(1),
    onNext: () => console.log('move'),
    onCancel: () => console.log('cancel'),
    onEnd: () => console.log('end')
  };
}

export function pan(stream, dispatch, getState) {
  var firstPos;
  stream.first().subscribe( pos => {
    firstPos = pos;
  });
  var { x, y, z } = getState().viewport;
  return {
    stream: stream.map((pos) =>
      firstPos.clone().subtract(pos).mulS(z).add({x, y})
    ),
    onNext: (trackPos) => dispatch(setCam({x: trackPos.x, y: trackPos.y, z})),
    onCancel: () => dispatch(setCam({x, y, z}))
  };

}

function getAbsPos(relPos, getState) {
  var { viewport: {w, h, x, y, z, cam} } = getState();
  if (window.camLock === true) {
    x = cam.x
    y = cam.y
  }
  return relPos.clone().subtract({x: w/2, y: h/2}).mulS(z).add({x, y});
}

const MAX_SNAP_DISTANCE = 6
function getSnappedPos(absPos, getState, ignoreLineID) {
  var { viewport: {z} } = getState();
  let track = getTrackFromCache(getState)

  // adjust snap radius to current zoom level
  var maxSnap = MAX_SNAP_DISTANCE * Math.max(z, ZOOM.MIN * 10)

  let point = track.getLinesInRadius(absPos.x, absPos.y, maxSnap)
    .filter(line =>
      // skip our ignoreline if given
      // because it represents the current line we're drawing
      line.id !== ignoreLineID
    )
    .reduce((points, line) => {
      // create array of points from array of lines
      let p = line.p.clone()
      let q = line.q.clone()
      p.line = line
      q.line = line
      return points.concat([p, q])
    }, [])
    .reduce(([snapPos, closestDistance], point) => {
      // reduce array of points to the point closest to absPos within maxSnap
      let distance = absPos.distance(point)
      return distance < closestDistance
        ? [point, distance]
        : [snapPos, closestDistance]
    }, [absPos, maxSnap])[0]
  let line = point.line
  point = point.clone() // vectors are mutable, defensively copy them
  point.line = line
  return point
}

const ZOOM = {
  STRENGTH: Math.pow(2, 1/(2<<5)),
  MAX: 2<<4,
  MIN: 1/(2<<4)
};
function getZoom(z, delta) {
  let dz = Math.pow(ZOOM.STRENGTH, delta);
  return Math.min(Math.max(z / dz, ZOOM.MIN), ZOOM.MAX);
}
function getPosFromZoom(pos, center, zoom, initZoom) {
  return pos.clone().mulS(-1).add(center).mulS(zoom / initZoom).add(pos)
}
export function zoom(stream, dispatch, getState) {
  var firstPos, y0;
  var { viewport: {x, y, z} } = getState();
  stream.first().subscribe( pos => {
    y0 = pos.y;
    firstPos = getAbsPos(pos, getState);
  });
  return {
    stream: stream.map(pos => getZoom(z, y0 - pos.y)),
    onNext: (zoom) => {
      let pos = getPosFromZoom(firstPos, {x, y}, zoom, z)
      dispatch(setCam({x: pos.x, y: pos.y, z: zoom}));
    },
    onCancel: () => dispatch(setCam({x, y, z}))
  };

}

export function deltaPanModZoom(pos, delta, dispatch, getState, pinch) {
  var { viewport: {x, y, z}, modKeys: {mod} } = getState();
  mod = mod || pinch
  let newPos
  let zoom = z
  if (mod) { // zoom
    zoom = getZoom(z, -delta.y) // backwards bc need to match pinch-to-zoom
    newPos = getPosFromZoom(getAbsPos(pos, getState), {x, y}, zoom, z)
  } else { // pan
    newPos = delta.mulS(z).add({x, y})
  }
  dispatch(setCam({
    x: newPos.x,
    y: newPos.y,
    z: zoom
  }))
}

// TODO: round according to zoom factor
const ANGLE_INC = 15
function angleSnap(pnt, pos) {
  let delta = pnt.clone().subtract(pos)
  let angle = Math.atan2(delta.y, delta.x) / Math.PI * 180
  angle = Math.round(angle / ANGLE_INC) * ANGLE_INC
  let [x, y] = (() => {
    switch (angle) {
      case 360:
      case 0:
        return [1, 0]
      case 45:
        return [Math.SQRT1_2, Math.SQRT1_2]
      case 90:
        return [0, 1]
      case 135:
        return [-Math.SQRT1_2, Math.SQRT1_2]
      case 180:
        return [-1, 0]
      case 225:
        return [-Math.SQRT1_2, -Math.SQRT1_2]
      case 270:
        return [0, -1]
      case 315:
        return [Math.SQRT1_2, -Math.SQRT1_2]
      default:
        let rads = angle / 180 * Math.PI
        return [Math.cos(rads), Math.sin(rads)]
    }
  })()
  return (new Vector(x, y)).mulS(delta.dot({x, y})).add(pos)
}
function angleLock(pnt, pos, angle) {
  let delta = pnt.clone().subtract(pos)
  let [x, y] = [Math.cos(angle), Math.sin(angle)]
  return (new Vector(x, y)).mulS(delta.dot({x, y})).add(pos)
}
const BASE_LENGTH = 10
function lengthSnap(pnt, pos) {
  let delta = pnt.clone().subtract(pos)
  let length = delta.length()
  length = BASE_LENGTH * Math.pow(2, Math.round(Math.log2(length / BASE_LENGTH)))
  delta.normalize().mulS(length)
  return delta.add(pos)
}
function lengthLock(pnt, pos, length) {
  let delta = pnt.clone().subtract(pos)
  delta.normalize().mulS(length)
  return delta.add(pos)
}

// TODO: put ID management in reducer
const getMinLineLength = getState => Math.max(4 * getState().viewport.z, 0.1)
export function line(stream, dispatch, getState) {
  let p1
  let prevLine = null
  let id = getState().trackData.maxLineID + 1;
  // TODO: wrap functions around mod keys for clarity
  stream = stream.map(pos => {
    let absPos = getAbsPos(pos, getState)
    let {modKeys: {alt, mod}} = getState()
    if (!alt && !mod) {
      return getSnappedPos(absPos, getState, prevLine ? prevLine.id : null)
    }
    return absPos
  })

  stream.first().subscribe( pos => {
    p1 = pos
  });
  let {modKeys: {shift: initShift}} = getState()

  return {
    stream: stream
      .filter(p2 => p1.distance(p2) >= getMinLineLength(getState)),
    onNext: (p2) => {
      let {toolbars: {colorSelected: lineType}, modKeys: {shift, mod, alt}} = getState()
      if (shift && mod) {
        if (alt) {
          p2 = angleSnap(p2, p1)
        }
        p2 = lengthSnap(p2, p1)
      } else if (mod) {
        p2 = angleSnap(p2, p1)
      } else if (shift) {
        if (p1.line) {
          let angle = Math.atan2(p1.line.vec.y, p1.line.vec.x)
          p2 = angleLock(p2, p1, angle)
        }
      }
      let lineData = {
        x1: p1.x,
        y1: p1.y,
        x2: p2.x,
        y2: p2.y,
        id: id,
        flipped: initShift,
        type: lineType
      }
      let action
      if (prevLine) {
        action = replaceLine(prevLine, lineData)
      } else {
        action = addLine(lineData)
      }
      dispatch(action)
      prevLine = lineData
    },
    onEnd: () => {
      if (prevLine) {
        dispatch(pushAction(addLine(prevLine)))
      }
    },
    onCancel: () => {
      if (prevLine) {
        dispatch(removeLine(prevLine))
      }
    }
  }
}
export function pencil(stream, dispatch, getState) {
  stream = stream.map((pos) => getAbsPos(pos, getState))
  let p0
  let addedLines = []
  stream.first().subscribe( pos => {
    p0 = pos
  });
  return {
    stream: stream
      .scan(([prevLine, p1 = p0], p2) => {
        if (p1.distance(p2) >= 2 * getMinLineLength(getState)) {
          return [[p1, p2], p2]
        }
        return [null, p1]
      }, [])
      .map(([prevLine, p1]) => prevLine)
      .filter(prevLine => prevLine !== null),
    onNext: ([p1, p2]) => {
      let {toolbars: {colorSelected: lineType}, modKeys: {shift}} = getState()
      let lineData = {
        x1: p1.x,
        y1: p1.y,
        x2: p2.x,
        y2: p2.y,
        flipped: shift,
        id: getState().trackData.maxLineID + 1,
        type: lineType
      }
      dispatch(addLine(lineData))
      addedLines.push(lineData);
    },
    onEnd: () => {
      if (addedLines.length > 0) {
        dispatch(pushAction(addLine(addedLines)))
      }
    },
    onCancel: () => {
      dispatch(removeLine(addedLines))
    }
  }
}

const ERASER_RADIUS = 2;
export function eraser(stream, dispatch, getState, cancellableStream) {
  var removedLines = [];
  return {
    stream: cancellableStream.map((pos) => getAbsPos(pos, getState)),
    onNext: (pos) => {
      let track = getTrackFromCache(getState)
      let linesToRemove = track.getLinesInRadius(pos.x, pos.y, ERASER_RADIUS)
      removedLines = removedLines.concat(linesToRemove);
      dispatch(removeLine(linesToRemove));
    },
    onEnd: () => {
      if (removedLines.length > 0) {
        dispatch(pushAction(removeLine(removedLines)))
      }
    },
    onCancel: () => {
      dispatch(addLine(removedLines))
    }
  }
}

const DragTypes = {
  P: 1,
  Q: 2,
  LINE: 3 // bit flags ok???
}
const SELECTION_RADIUS = 10
function getDragType(pos, line, radius) {
  if (line.vec.clone().mulS(0.5).add(line.p).distance(pos) < (radius / 2)) {
    return DragTypes.LINE
  }
  let pDist = line.p.distance(pos)
  let qDist = line.q.distance(pos)
  if (pDist > radius && qDist > radius) {
    return DragTypes.LINE
  }
  if (pDist < qDist) {
    return DragTypes.P
  } else {
    return DragTypes.Q
  }
}

// TODO: separate into separate streams and clean up
export function select(stream, dispatch, getState, cancellableStream) {
  let firstPos
  let selectedLineID = null
  let initLine
  let prevLine = null
  let modifyingLine = null
  let dragType
  let {viewport: {z}, lineSelection: {lineID: prevSelectedLineID}} = getState()
  let track = getTrackFromCache(getState)
  let radius = SELECTION_RADIUS * z
  stream = cancellableStream.map((pos) => getAbsPos(pos, getState))
  stream.first().subscribe(pos => {
    firstPos = pos
    if (prevSelectedLineID != null) {
      let line = track.getLineByID(prevSelectedLineID)
      if (line && line.inRadius(pos.x, pos.y, radius)) {
        modifyingLine = line
        prevLine = line
        initLine = line
        dragType = getDragType(pos, line, radius)
        selectedLineID = prevSelectedLineID
        return
      }
    }
    let selectedLine = track.getLinesInRadius(pos.x, pos.y, radius)[0]
    if (selectedLine) {
      selectedLineID = selectedLine.id
    }
  })
  return {
    stream: stream.skip(1),
    onNext: (pos) => {
      if (modifyingLine != null) {
        let {modKeys: {mod, alt, shift}} = getState()
        let delta = pos.clone().subtract(firstPos)
        let {p, q} = modifyingLine
        let angle = Math.atan2(q.y - p.y, q.x - p.x)
        if (dragType & DragTypes.P) {
          p = p.clone().add(delta)
        }
        if (dragType & DragTypes.Q) {
          q = q.clone().add(delta)
        }
        if (shift && mod) {
          if (alt) {
            if (dragType === DragTypes.P) {
              p = angleSnap(p, q)
            }
            if (dragType === DragTypes.Q) {
              q = angleSnap(q, p)
            }
          }
          if (dragType === DragTypes.P) {
            p = lengthLock(p, q, initLine.length)
          }
          if (dragType === DragTypes.Q) {
            q = lengthLock(q, p, initLine.length)
          }
        } else if (mod) {
          if (dragType === DragTypes.P) {
            p = angleSnap(p, q)
          }
          if (dragType === DragTypes.Q) {
            q = angleSnap(q, p)
          }
        } else if (shift) {
          if (dragType === DragTypes.P) {
            p = angleLock(p, q, angle)
          }
          if (dragType === DragTypes.Q) {
            q = angleLock(q, p, angle)
          }
          if (dragType === DragTypes.LINE) {
            let parallel = angleLock(pos, firstPos, angle)
            let perpendicular = angleLock(pos, firstPos, angle + Math.PI / 2)
            if (parallel.distance(pos) < perpendicular.distance(pos)) {
              p = angleLock(p, initLine.p, angle)
              q = angleLock(q, initLine.q, angle)
            } else {
              p = angleLock(p, initLine.p, angle + Math.PI / 2)
              q = angleLock(q, initLine.q, angle + Math.PI / 2)
            }
          }
        }
        if (!alt && !mod && !shift) {
          let snap
          if (dragType === DragTypes.P) {
            snap = getSnappedPos(p, getState, selectedLineID)
            if (!snap.equals(q)) {
              p = snap
            }
          }
          if (dragType === DragTypes.Q) {
            snap = getSnappedPos(q, getState, selectedLineID)
            if (!snap.equals(p)) {
              q = snap
            }
          }
        }
        if (p.equals(q)) {
          return
        }
        let draggedLine = modifyingLine.setPoints(p, q)
        dispatch(replaceLine(prevLine, draggedLine))
        prevLine = draggedLine
      }
    },
    onEnd: () => {
      if (modifyingLine != null) {
        if (modifyingLine.equals(prevLine)) {
          return;
        }
        dispatch(pushAction(replaceLine(modifyingLine, prevLine)))
      } else {
        dispatch(selectLine(selectedLineID))
      }
    },
    onCancel: () => {
      if (modifyingLine != null) {
        dispatch(replaceLine(prevLine, modifyingLine))
      }
    }
  }
}
