'use strict';

import {last, without} from 'lodash'
import { getButtons } from './buttons';
import { setModKey, togglePlayPause, selectColor, deleteSelection, incFrameIndex, decFrameIndex } from './actions'
import {
  copyLine,
  pasteLine,
  nudgeLine,
  flipLine,
  duplicateLine,
  toggleExtension,
  setType,
  Direction,
  Entity,
  Orientation,
  Amount
  } from './lineEditing'

const useMeta = /Mac|iPod|iPhone|iPad/.test(navigator.platform)
const modifierRegex = /mod|alt/;

function getLeftRight (e) {
  let { ctrlKey, metaKey, altKey } = e
  let left = useMeta ? altKey : ctrlKey
  let right = useMeta ? metaKey : altKey
  return {left, right}
}

function bindModKeys(combokeys, dispatch) {
  let modKeys = ['shift', 'mod', 'alt']

  modKeys.forEach(key => {
    combokeys.bind(key, () => dispatch(setModKey(key, true)), 'keydown')
    combokeys.bind(key, () => dispatch(setModKey(key, false)), 'keyup')
  })

  // go through every combination
  let setComboModKeys = (keys, combo) => {
    if (combo.length > 1) {
      combokeys.bind(combo.join('+'), () => dispatch(setModKey(last(combo), true)), 'keypress')
    }
    if (keys.length > 0) {
      keys.forEach(key => {
        setComboModKeys(without(keys, key), combo.concat([key]))
      })
    }
  }
  setComboModKeys(modKeys, [])

  window.addEventListener('blur', () => {
    modKeys.forEach(key => dispatch(setModKey(key, false)))
  })
}

export default function bindHotkey(combokeys, ripples, name, hotkey, dispatch) {
  bindModKeys(combokeys, dispatch)

  let buttons = getButtons(dispatch);
  let boundAction, pressAction, releaseAction
  if (buttons[name]) {
    ({boundAction, pressAction, releaseAction} = buttons[name])
  }

  let startRipple = () => {};
  let endRipple = () => {};
  if (ripples[name]) {
    startRipple = () => ripples[name].forEach( ({start}) => start() );
    endRipple = () => ripples[name].forEach( ({end}) => end() );
  }

  if (modifierRegex.test(hotkey)) {
    // TODO: hack in multiple keyup binding
    combokeys.bind(hotkey, (e) => {
      e.preventDefault();
      if (boundAction) {
        boundAction()
      }
      startRipple();
      requestAnimationFrame(() =>
        requestAnimationFrame(endRipple)
      );
    }, 'keydown');
  } else {
    var rippled = false;
    combokeys.bind(hotkey, (e) => {
      if (pressAction) {
        pressAction()
      }
      if (!rippled) {
        startRipple();
        rippled = true;
      }
    }, 'keydown');
    combokeys.bind(hotkey, (e) => {
      if (boundAction) {
        boundAction()
      }
      if (releaseAction) {
        releaseAction()
      }
      endRipple();
      rippled = false;
    }, 'keyup');
  }

  combokeys.bind('space', e => {
    dispatch(togglePlayPause())
  })
  ;[0, 1, 2].forEach(i => {
    combokeys.bind((i+1).toString(), e => {
      dispatch(selectColor(i))
      dispatch(setType(i))
    })
  })

  combokeys.bind('backspace', e => {
    e.preventDefault()
    dispatch(deleteSelection())
  })

  const bindLeftRight = (key, cb) => {
    combokeys.bind(key, cb)
    combokeys.bind(`alt+${key}`, cb)
    combokeys.bind(`mod+${key}`, cb)
    combokeys.bind(`shift+${key}`, cb)
    combokeys.bind(`alt+shift+${key}`, cb)
    combokeys.bind(`mod+shift+${key}`, cb)
  }

  let nudgeKeys = [{
    direction: Direction.UP,
    absoluteKey: 'up',
    relativeKey: 'i'
  }, {
    direction: Direction.DOWN,
    absoluteKey: 'down',
    relativeKey: 'k'
  }, {
    direction: Direction.LEFT,
    absoluteKey: 'left',
    relativeKey: 'j',
    relativeJump: '['
  }, {
    direction: Direction.RIGHT,
    absoluteKey: 'right',
    relativeKey: 'l',
    relativeJump: ']'
  }]

  for (let {direction, absoluteKey, relativeKey, relativeJump} of nudgeKeys) {
    let keyCb = (orientation, jump = false) => e => {
      e.preventDefault()
      let { shiftKey } = e
      let { left, right } = getLeftRight(e)
      dispatch(nudgeLine({
        entity: left ? Entity.LEFT_POINT : right ? Entity.RIGHT_POINT : Entity.LINE,
        direction,
        orientation,
        amount: jump ? Amount.JUMP : shiftKey ? Amount.FINE : Amount.COARSE
      }))
    }
    bindLeftRight(absoluteKey, keyCb(Orientation.ABSOLUTE))
    bindLeftRight(relativeKey, keyCb(Orientation.RELATIVE))
    relativeJump && bindLeftRight(relativeJump, keyCb(Orientation.RELATIVE, true))
  }

  combokeys.bind('9', e => {
    e.preventDefault()
    dispatch(toggleExtension(Entity.LEFT_POINT))
  })
  combokeys.bind('0', e => {
    e.preventDefault()
    dispatch(toggleExtension(Entity.RIGHT_POINT))
  })
  combokeys.bind('-', e => {
    e.preventDefault()
    dispatch(flipLine())
  })
  combokeys.bind('=', e => {
    e.preventDefault()
    dispatch(duplicateLine())
  })

  combokeys.bind('mod+c', e => {
    e.preventDefault()
    dispatch(copyLine())
  })
  combokeys.bind('mod+v', e => {
    e.preventDefault()
    dispatch(pasteLine())
  })

  combokeys.bind('mod+,', e => {
    e.preventDefault()
    dispatch(decFrameIndex())
  })
  combokeys.bind('mod+.', e => {
    e.preventDefault()
    dispatch(incFrameIndex())
  })
}
