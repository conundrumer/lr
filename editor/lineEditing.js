import Vector from 'core/Vector'

import { replaceLine, addLine, pushAction, selectLine } from './actions'
import { getTrackFromCache } from './trackCacheMiddleware'
import { camSelector } from './selectors'

export const Direction = {
  UP: new Vector(0, -1),
  DOWN: new Vector(0, 1),
  LEFT: new Vector(-1, 0),
  RIGHT: new Vector(1, 0)
}
export const Entity = {
  LINE: [1, 1],
  LEFT_POINT: [1, 0],
  RIGHT_POINT: [0, 1]
}
export const Orientation = {
  RELATIVE: true,
  ABSOLUTE: false
}
export const Amount = {
  JUMP: 10,
  COARSE: 1,
  FINE: 1 / 16
}

function getDelta (nudgeProps, line, z) {
  let { direction, entity, orientation, amount } = nudgeProps
  let d = direction.clone().mulS(amount)

  if (line.flipped) {
    entity = entity === Entity.LEFT_POINT ? Entity.RIGHT_POINT : entity === Entity.RIGHT_POINT ? Entity.LEFT_POINT : entity
  }

  if (orientation === Orientation.RELATIVE) {
    let vec = line.vec.clone().normalize().mulS(line.flipped ? -1 : 1)
    d = new Vector(
      d.x * vec.x - d.y * vec.y,
      d.x * vec.y + d.y * vec.x
    )
    let left = entity === Entity.LEFT_POINT
    let right = entity === Entity.RIGHT_POINT
    if ((left || right) && (direction === Direction.UP || direction === Direction.DOWN)) {
      let v = line.vec.clone().mulS(left ? -1 : 1)
      let dv = v.clone().add(d).normalize().mulS(v.length())
      d = dv.clone().subtract(v)
    }
  }

  return [d.clone().mulS(entity[0]), d.clone().mulS(entity[1])]
}

function getSelectedLine (getState) {
  let {
    lineSelection: {lineID: selectedLineID}
  } = getState()

  let track = getTrackFromCache(getState)
  if (selectedLineID == null) {
    return
  }
  return track.getLineByID(selectedLineID)
}

export function nudgeLine (nudgeProps) {
  return (dispatch, getState) => {
    let {
      viewport: {z}
    } = getState()

    let line = getSelectedLine(getState)
    if (!line) {
      return
    }

    let delta = getDelta(nudgeProps, line, z)

    let action = replaceLine(line, line.setPoints(delta[0].add(line.p), delta[1].add(line.q)))
    dispatch(action)
    dispatch(pushAction(action))
  }
}

export function toggleExtension (point) {
  return (dispatch, getState) => {
    let line = getSelectedLine(getState)
    if (!line) {
      return
    }
    if (line.flipped) {
      point = point === Entity.LEFT_POINT ? Entity.RIGHT_POINT : point === Entity.RIGHT_POINT ? Entity.LEFT_POINT : point
    }

    let nextLine
    switch (point) {
      case Entity.LEFT_POINT:
        nextLine = line.edit({ leftExtended: !line.leftExtended })
        break
      case Entity.RIGHT_POINT:
        nextLine = line.edit({ rightExtended: !line.rightExtended })
        break
    }
    if (nextLine) {
      let action = replaceLine(line, nextLine)
      dispatch(action)
      dispatch(pushAction(action))
    }
  }
}

export function setType (type) {
  return (dispatch, getState) => {
    let line = getSelectedLine(getState)
    if (!line) {
      return
    }

    let nextLine = line.edit({ type })

    let action = replaceLine(line, nextLine)
    dispatch(action)
    dispatch(pushAction(action))
  }
}

export function flipLine () {
  return (dispatch, getState) => {
    let line = getSelectedLine(getState)
    if (!line) {
      return
    }

    let nextLine = line.edit({ p: line.q, q: line.p, flipped: !line.flipped })

    let action = replaceLine(line, nextLine)
    dispatch(action)
    dispatch(pushAction(action))
  }
}

export function duplicateLine () {
  return (dispatch, getState) => {
    let line = getSelectedLine(getState)
    if (!line) {
      return
    }

    let nextLine = line.edit({})
    nextLine.id = getState().trackData.maxLineID + 1

    let action = addLine(nextLine)
    dispatch(action)
    dispatch(pushAction(action))
  }
}

window.clipboard = {line: null, cam: null}
export function copyLine () {
  return (dispatch, getState) => {
    let line = getSelectedLine(getState)
    if (!line) {
      return
    }

    window.clipboard = {
      line: line,
      cam: camSelector(getState())
    }
  }
}

export function pasteLine () {
  return (dispatch, getState) => {
    let {line, cam} = window.clipboard
    if (!line) {
      return
    }
    let nextCam = camSelector(getState())

    let p = line.p.clone().subtract(cam).add(nextCam)
    let q = line.q.clone().subtract(cam).add(nextCam)

    let nextLine = line.setPoints(p, q)
    nextLine.id = getState().trackData.maxLineID + 1

    let action = addLine(nextLine)
    dispatch(action)
    dispatch(pushAction(action))
    dispatch(selectLine(nextLine.id))
  }
}
