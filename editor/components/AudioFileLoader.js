import React from 'react'
import PureComponent from 'react-pure-render/component';
import { Dialog, CircularProgress } from 'material-ui'
import Dropzone from 'react-dropzone'
import { hideAudioFileLoader, loadAudioFile } from '../actions'

import '../styles/FileLoader.less'

export default class FileLoader extends PureComponent {

  render() {
    return (
      <Dialog
        ref={component => this.dialog = component}
        open={this.props.open}
        title='Load Audio From File'
        modal={true}
        actions={[{ text: 'Cancel', onTouchTap: () => this.props.dispatch(hideAudioFileLoader()) }]}
      >
        <Dropzone
          className='file-loader-dropzone'
          activeClassName='file-loader-dropzone active'
          onDrop={file => this.props.dispatch(loadAudioFile(file))}
          multiple={false}
        >
          {
            this.props.loadingFile ? [
              <CircularProgress key={0} mode='indeterminate' />,
              <p key={1}>Loading file</p>
            ] : [
              <p key={2}>{'Drag and drop the audio file into this box'}</p>,
              <p key={3}>Or click in this box to select the audio file</p>
            ].concat(this.props.error ? [
              <p key={4}>Something wrong happened:</p>,
              <p key={5}><i>{this.props.error}</i></p>
            ] : [])
          }

        </Dropzone>
      </Dialog>
    )
  }

}
