import React from 'react'
import Popout from 'react-popout'
import Whammy from 'whammy'
import {saveAs} from 'filesaverjs'
import { debounce } from 'lodash'

import {stopRecording, setPlaybackState, setFrameMaxIndex} from '../actions'

function randomString (length, pool) {
  var text = ""
  for (var i = 0; i < length; i++) {
    text += pool.charAt(Math.floor(Math.random() * pool.length))
  }
  return text
}

const SIZE_RATIO = 6 / 8 // base 64 uses 6 bits out of 8
const SIZE_LIMIT = 500 * 1000 * 1000 / 2 // 500 mb for chrome, halved to alternate memory allocation
const QUALITY = 0.99 // force using lossy webp for webm compatibility

export default class Recorder extends React.Component {

  constructor () {
    super()
    this.images = []
  }

  onUpdate () {
    let display = this.props.getDisplay()
    display.renderToCanvas(this.canvas, this.img, () => {
      let data = this.canvas.toDataURL('image/webp', QUALITY)
      this.images.push(data)
    })
  }

  componentDidMount () {
    this.endIndex = this.props.maxIndex
    let {width, height} = this.props
    this.canvas = document.createElement('canvas')
    this.canvas.width = width;
    this.canvas.height = height;
    this.img = document.createElement('img')
    this.img.width = width
    this.img.height = height

    this.props.dispatch(setPlaybackState('stop'))
    setTimeout(() => {
      this.props.dispatch(setPlaybackState('play'))
      this.onUpdate()
    }, 1000)
  }

  componentWillUnmount() {
    this.props.dispatch(setPlaybackState('stop'))
    this.props.dispatch(setFrameMaxIndex(this.endIndex)) // wooo hack
    if (this.images.length > 0) {
      let [, ...chunks] = this.images.reduce(([currentSize, currentChunk, ...prevChunks], image) => {
        let imageSize = image.length * SIZE_RATIO
        let nextSize = currentSize + imageSize
        if (nextSize < SIZE_LIMIT) {
          return [nextSize, [...currentChunk, image], ...prevChunks]
        } else {
          return [imageSize, [image], currentChunk, ...prevChunks]
        }
      }, [0, []])
      let n = chunks.length
      let name = randomString(3, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
      setTimeout(function exportVideoChunk (i = 1) {
        let win = window.open('', 'Video Preview')
        win.document.body.innerHTML = `
          <style>
            html, body { background-color: black; }
            h1 { color: white; }
          </style>
          <h1>generating chunk ${i} of ${n}...</h1>
        `
        setTimeout(() => {
          try {
            let blob = Whammy.fromImageArray(chunks.pop(), window.FPS)
            let vid = window.URL.createObjectURL(blob)
            win.document.body.innerHTML = `
              <style>
                html, body { height: 100%; margin: 0; padding: 0; background-color: black; }
                button { position: absolute; top: 0; }
                #downloadButton { left: 0; }
                #nextButton { right: 0; }
                #vid { width: 100%; height: 100% }
              </style>
              <video id="vid" src=${vid} autoplay></video>
              <button id="downloadButton">Download video ${i} of ${n}</button>
              <button id="nextButton">Continue</button>
            `
            win.downloadButton.onclick = () => saveAs(blob, `recording-${name}-${i}.webm`)
            win.nextButton.onclick = () => {
              window.URL.revokeObjectURL(vid)
              if (chunks.length > 0) {
                exportVideoChunk(i + 1)
              } else {
                win.close()
              }
            }
            const controlOff = debounce(() => {
              if (win && win.vid && win.vid.controls != null) {
                win.vid.controls = false
              }
            }, 1500)
            let mouseActive = () => {
              win.vid.controls = true
              controlOff()
            }
            win.vid.onmousedown = mouseActive
            win.vid.onmousemove = mouseActive
            win.vid.onmouseup = mouseActive
          } catch (e) {
            console.error(e)
            win.document.body.innerHTML = `
              <h1>Something wrong happened: ${e}</h1>
            `
          }
        }, 100)
      }, 0)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.index > this.endIndex + 1) { // lol hack
      this.onUpdate()
      this.props.dispatch(stopRecording())
    } else if (nextProps.index > this.props.index) {
      this.onUpdate()
    }
  }

  render () {
    let displayComponent = this.props.children
    return (
      <Popout
        title='Recording track (close window to stop recording)'
        onClosing={() => this.props.dispatch(stopRecording())}
        window={{open: (...args) => {
          let win = window.open(...args)
          let css = win.document.createElement('style')
          css.innerText = 'html, body { margin: 0; padding: 0; }'
          win.document.head.appendChild(css)
          return win
        }}}
        options={{
          width: this.props.width,
          height: this.props.height
        }}>
        {displayComponent}
      </Popout>
    )
  }
}
