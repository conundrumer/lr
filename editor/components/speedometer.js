import React from 'react'

import Vector from 'core/Vector'
function angleTo (u, v) {
  return Math.atan2(u.cross(v), u.dot(v))
}
export default function Speedometer ({rider, prevRider, toolbarsOpen}) {
  let style = {position: 'absolute', top: toolbarsOpen ? 40 : 0, right: 0, padding: 8, textAlign: 'right', fontFamily: 'monospace', backgroundColor: 'rgba(255,255,255,0.8)'}

  let avgPos = rider.points.reduce((acc, {pos}) => acc.add(pos), new Vector(0, 0)).divS(rider.points.length)
  let avgPrevPos = prevRider.points.reduce((acc, {pos}) => acc.add(pos), new Vector(0, 0)).divS(rider.points.length)
  let avgVel = avgPos.clone().subtract(avgPrevPos)
  let rotVel = rider.points.map(({pos}, i) =>
    angleTo(pos.clone().subtract(avgPos), prevRider.points[i].pos.clone().subtract(avgPrevPos))
  ).reduce((acc, angle) => acc + angle, 0) / rider.points.length
  let strain = rider.constraints.reduce((acc, s) => acc + s, 0)

  if (rider.crashed) {
    avgVel = rider.position.clone().subtract(prevRider.position)
  }
  return (
    <div style={style}>
      {avgVel.length().toFixed(3)} p/f<br/>
      {(Math.atan2(avgVel.y, avgVel.x) / Math.PI * -180).toFixed(2)}˚<br/>
      {rider.crashed ? 'crashed' : (
        <div>
          {(rotVel / Math.PI * 180).toFixed(2)}˚/f<br/>
          {strain.toFixed(3)} p
        </div>
      )}
    </div>
  )
}
