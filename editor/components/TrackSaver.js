// import download from 'downloadjs'
import {saveAs} from 'filesaverjs'
import React, { PropTypes } from 'react'
import PureComponent from 'react-pure-render/component';
import { Dialog, FlatButton, RaisedButton, TextField, Checkbox } from 'material-ui'
import { hideTrackSaver, setTrackName } from '../actions'
import { jsonWriter, solWriter } from 'io'

import '../styles/TrackSaver.less'

export default class TrackSaver extends PureComponent {

  static get propTypes() {
    return {
      dispatch: PropTypes.func.isRequired,
      open: PropTypes.bool.isRequired,
      trackData: PropTypes.object,
      label: PropTypes.string,
      fileName: PropTypes.string
    }
  }

  download () {
    if (this.refs.legacy.isChecked()) {
      saveAs(
        new Blob([solWriter(this.props.trackData)], {
          type: 'application/octet-stream'
        }),
        this.props.fileName + '.savedLines.sol'
      )
    } else {
      saveAs(
        new Blob([jsonWriter(this.props.trackData)], {
          type: 'application/octet-stream'
        }),
        this.props.fileName + '.track.json'
      )
    }
  }

  getActions() {
    return [
      <RaisedButton style={{margin: 10}} primary={true} label='Save To File' onTouchTap={() => this.download()} />,
      <RaisedButton disabled={true} style={{margin: 10}} primary={true} key={1} label='Save To Pastebin*' onTouchTap={() => {}} />,
      <FlatButton secondary={true} key={2} label='Done' onTouchTap={() => this.props.dispatch(hideTrackSaver())} />
    ]
  }

  onClickJson() {
    let range = document.createRange();
    range.selectNode(this.codeBlock);
    window.getSelection().addRange(range);
  }

  render() {
    return (
      <Dialog
        ref={component => this.dialog = component}
        title='Save Track'
        open={this.props.open}
        modal={true}
        actions={this.getActions()}
        autoScrollBodyContent={true}
      >
        <TextField
          floatingLabelText='Track Name'
          defaultValue={this.props.label}
          onChange={e => this.props.dispatch(setTrackName(e.target.value))}
        />
        <Checkbox ref='legacy' label='Use legacy file format'/>
      </Dialog>
    )
  }

}
