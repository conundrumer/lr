import React from 'react'
import PureComponent from 'react-pure-render/component'
import Markdown from 'react-remarkable'

import { Dialog, CircularProgress } from 'material-ui'
import { getReleaseInfo, showFileLoader } from '../actions'

import { version } from 'json!../../package.json'

class Release extends PureComponent {
  render () {
    let {name, tag, body, url, date} = this.props
    return (
      <li>
        <h4><a href={url} target='_blank'>{tag}</a> {date} {name}</h4>
        <Markdown>
          {body}
        </Markdown>
      </li>
    )
  }
}

export default class LaunchModal extends PureComponent {

  constructor () {
    super()
    this.state = { open: true }
  }

  componentDidMount () {
    this.props.dispatch(getReleaseInfo())
  }

  render () {
    return (
      <Dialog
        ref='dialog'
        title={'Line Rider JS v' + version}
        open={this.state.open && this.props.initOpen}
        autoScrollBodyContent={true}
        actions={[{
          text: 'Load Track',
          onTouchTap: () => {
            this.setState({open: false})
            this.props.dispatch(showFileLoader())
          }
        }, {
          text: 'Play',
          ref: 'play'
        }]}
        onRequestClose={() => this.setState({open: false})}
        actionFocus='play'
      >
        <p>This version of Line Rider is no longer supported! The exported files from this version may be incompatible with other versions of Line Rider! Play the most up-to-date version of Line Rider at <a href="https://www.linerider.com">www.linerider.com</a></p>
      </Dialog>
    )
  }

}
