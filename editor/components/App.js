'use strict';

var React = require('react');
import ReactDOM from 'react-dom';
var { connect } = require('react-redux');

import mui from 'material-ui'
let {
  Styles: {
    ThemeManager,
    LightRawTheme,
    Colors: {
      blue500,
      red500,
      green500
    }
  }
} = mui

var { Display } = require('renderer');

var { setWindowSize } = require('../actions');
var Editor = require('./Editor');
import FileLoader from './FileLoader';
import AudioFileLoader from './AudioFileLoader';
import TrackSaver from './TrackSaver'
import LaunchModal from './LaunchModal'
import Recorder from './Recorder'
import Speedometer from './speedometer'

import select from '../selectors'
import Combokeys from 'combokeys'
import { StartFlagRaw as StartFlag, FlagRaw as Flag } from './SvgIcons'

var BLOCK_CONTEXT_MENU = true;

function getTheme() {
  let rawTheme = LightRawTheme
  let muiTheme = ThemeManager.getMuiTheme(rawTheme)
  muiTheme = ThemeManager.modifyRawThemePalette(muiTheme, {
    primary1Color: '#3995FD',
    primary2Color: '#FD4F38',
    primary3Color: '#06A725'
  })
  return {
    ...muiTheme,
    raisedButton: {
      ...muiTheme.raisedButton,
      primaryColor: red500
    },
    menuItem: {
      ...muiTheme.menuItem,
      selectedTextColor: red500
    },
    floatingActionButton: {
      ...muiTheme.floatingActionButton,
      miniSize: 20
    }
  }
}

var App = React.createClass({

  getChildContext() {
    return {
      muiTheme: getTheme()
    };
  },

  get childContextTypes() {
    return {
      muiTheme: React.PropTypes.object
    }
  },

  componentWillMount() {
    this.combokeys = new Combokeys(document);
  },

  componentDidMount() {
    this.interval = setInterval(this.onResize, 100);
  },

  componentWillUnmount() {
    clearInterval(this.interval);
    this.combokeys.detach();
  },

  onResize() {
    let {w: prevWidth, h: prevHeight} = this.props.widthHeight;
    // let {width, height} = this.container.getBoundingClientRect();
    let {
      innerWidth: width,
      innerHeight: height
    } = window;

    if (width > 0 && height > 0 && width !== prevWidth || height !== prevHeight) {
      this.props.dispatch(setWindowSize({width, height}));
    }
  },

  render() {
    let {
      dispatch,
      fileLoader,
      audioFileLoader,
      timeline,
      editor,
      trackSaver,
      display,
      drawingSurface,
      splashModal,
      recording,
      recorder
    } = this.props

    let displayComponent = (
      <Display ref='display'
        {...display}
        {...{display}}
        startIcon={<StartFlag color='rgba(0,0,0,0.4)' />}
        flagIcon={<Flag color='rgba(0,0,0,0.4)' />}
        endIcon={null}
        useCanvas
      />
    )

    return (
      <div
        className='main'
        ref={component => this.container = ReactDOM.findDOMNode(component)}
        onContextMenu={e => BLOCK_CONTEXT_MENU ? e.preventDefault() : null}
      >
        {!recording ? displayComponent :
          <Recorder {...recorder} dispatch={dispatch} getDisplay={() => this.refs.display}>
            {displayComponent}
          </Recorder>
        }
        <Editor {...editor} {...{editor, fileLoader, trackSaver, timeline, drawingSurface}} combokeys={this.combokeys} dispatch={dispatch} />
        { window.speed && <Speedometer rider={display.rider} prevRider={display.prevRider} toolbarsOpen={editor.toolbarsOpen} />}
        <FileLoader {...fileLoader} dispatch={dispatch} />
        <AudioFileLoader {...audioFileLoader} dispatch={dispatch} />
        <TrackSaver {...trackSaver} dispatch={dispatch} />
        <LaunchModal {...splashModal} dispatch={dispatch} />
      </div>
    );
  }
});

module.exports = connect(select)(App);
