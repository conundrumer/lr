import _ from 'lodash'
import { createSelector, createStructuredSelector } from 'reselect';
import { getTrackFromCache } from './trackCacheMiddleware'

const createSelectorFromProps = (reducer, props) =>
  createSelector(
    props.map(prop =>
      state => state[reducer][prop]
    ),
    (...args) => {
      let out = {}
      props.forEach((prop, i) =>
        out[prop] = args[i]
      )
      return out
    }
  )

const inPlaybackModeSelector = ({playback: {state}}) => state !== 'stop' && state !== 'pause'

// track gets mutated but startPosition and lineStore are immutable
// const trackSelector = createSelectorFromProps('trackData', ['track', 'startPosition', 'lineStore'])
const trackSelector = createSelector(
  [
    state => state.trackData.startPosition,
    state => state.trackData.lineStore
  ],
  // the track from cache is derived purely from startPosition and lineStore
  (startPosition, lineStore) => ({
    track: getTrackFromCache(null, lineStore, startPosition),
    startPosition,
    lineStore
  })
)
const viewportSelector = createSelectorFromProps('viewport', ['w', 'h', 'x', 'y', 'z'])
const fileLoaderSelector = createSelectorFromProps('fileLoader', ['open', 'loadingFile', 'error', 'fileName', 'tracks'])
const audioFileLoaderSelector = createSelectorFromProps('audioFileLoader', ['open', 'loadingFile', 'error'])
const toolbarSelector = createSelectorFromProps('toolbars', ['toolbarsOpen', 'timeControlOpen', 'sidebarOpen', 'sidebarSelected', 'colorSelected'])

const widthHeightSelector = createSelector(
  [
    viewportSelector,
    (state) => state.recording
  ],
  ({w, h}, recording) => ({
    w: !recording ? w : window.recorder.width,
    h: !recording ? h : window.recorder.height
  })
)

export const playbackCamSelector = createSelector(
  [
    state => state.viewport.cam,
    viewportSelector,
    state => state.playback.index
  ],
  ({x, y}, {z}, index) => {
    /* temporary console auto-zoom */
    z = window.getAutoZoom && window.getAutoZoom(index) || z
    /* /temporary console auto-zoom */
    return {x, y, z}
  }
)
export const camSelector = createSelector(
  [
    viewportSelector,
    state => (window.camLock === undefined ? inPlaybackModeSelector(state) : window.camLock) ? playbackCamSelector(state) : null
  ],
  ({x, y, z}, playbackCam) => {
    if (playbackCam) {
      return playbackCam
    }
    return {x, y, z}
  }
)

const lineSelector = createSelector(
  [
    trackSelector
  ],
  ({track}) => {
    return track.store.linesList;
  }
)

window.MAX_ONION_FRAMES = 10 * 40
const indexSelector = createSelector(
  [
    () => window.MAX_ONION_FRAMES,
    state => state.playback.index,
    state => state.playback.flag,
  ],
  (MAX_ONION_FRAMES, index, flagIndex) => {
    let clampedFlagIndex
    if (flagIndex > index) {
      clampedFlagIndex = index + Math.min(MAX_ONION_FRAMES, flagIndex - index)
    } else {
      clampedFlagIndex = index - Math.min(MAX_ONION_FRAMES, index - flagIndex)
    }
    return {
      index,
      flagIndex,
      startIndex: Math.min(index, clampedFlagIndex),
      endIndex: Math.max(index, clampedFlagIndex)
    }
  }
)
const riderSelector = createSelector(
  [
    indexSelector,
    trackSelector
  ],
  ({index, flagIndex, startIndex, endIndex}, {track}) => ({
    startPosition: track.getStartPosition(),
    rider: track.getRiderStateAtFrame(index),
    prevRider: track.getRiderStateAtFrame(Math.max(index - 1, 0)),
    flagRider: track.getRiderStateAtFrame(flagIndex),
    states: _.range(startIndex, endIndex + 1)
      .map(i => track.getRiderStateAtFrame(i))
  })
)

const colorPickerOpenSelector = state => {
  switch (state.selectedTool) {
    case 'pencil':
    case 'line':
    case 'curve':
    case 'brush':
    case 'multiLine':
      return true
    default:
      return false
  }
}

const selectedSelector = createSelector(
  [
    state => state.toggled,
    state => state.selectedTool,
    state => state.playback.state
  ],
  (toggled, selectedTool, playbackState) => ({
    ...toggled,
    [selectedTool]: true,
    [playbackState]: playbackState !== 'stop'
  })
)

const editorSelector = createSelector(
  [
    toolbarSelector,
    inPlaybackModeSelector,
    selectedSelector,
    colorPickerOpenSelector,
    (state) => state.audio.enabled
  ],
  (toolbars, inPlaybackMode, selected, colorPickerOpen, audioEnabled) => ({
    ...toolbars,
    inPlaybackMode,
    selected,
    audioEnabled,
    colorPickerOpen: colorPickerOpen && !inPlaybackMode
  })
)

const timelineSelector = createSelector(
  [
    state => state.playback.index,
    state => state.playback.flag,
    state => state.playback.maxIndex
  ],
  (index, flagIndex, maxIndex) => ({index, flagIndex, maxIndex})
)

// TODO: make sidebar selector
// TODO: move rendering button logic to here
const viewOptionsSelector = createSelector(
  [
    inPlaybackModeSelector,
    state => state.toggled.showContactPoints
  ],
  (inPlaybackMode, showContactPoints) => ({
    color: (window.colorLock === undefined) ? !inPlaybackMode : window.colorLock,
    floor: true,
    accArrow: true,
    snapDot: true,
    showContactPoints
  })
)

const lineSelectionSelector = createSelector(
  [
    trackSelector,
    state => state.lineSelection.lineID,
    state => state.selectedTool
  ],
  ({track}, lineID, tool) => {
    if (tool !== 'select') {
      return []
    }
    if (lineID != null) {
      let line = track.getLineByID(lineID)
      if (line != null) {
        return [line]
      }
    }
    return []
  }
)

const displaySelector = createSelector(
  [
    timelineSelector,
    indexSelector,
    riderSelector,
    camSelector,
    lineSelector,
    widthHeightSelector,
    viewOptionsSelector,
    state => state.toggled.onionSkin || false,
    lineSelectionSelector,
    (state) => state.recording,
    trackSelector,
    playbackCamSelector
  ],
  ({index, flagIndex, maxIndex}, {startIndex, endIndex}, {startPosition, states, rider, prevRider, flagRider}, cam, lines, {w, h}, viewOptions, onionSkin, lineSelection, recording, {track}, playbackCam) => ({
    frame: index,
    flagIndex,
    maxIndex,
    startPosition,
    rider,
    prevRider,
    flagRider,
    riders: states,
    startIndex,
    endIndex,
    cam,
    lines,
    width: w,
    height: h,
    viewOptions,
    onionSkin,
    lineSelection,
    hideFlags: recording,
    resolution: recording ? 1 : undefined,
    track: track,
    playbackCam
  })
)

const trackSaverSelector = createSelector(
  [
    trackSelector,
    state => state.trackSaver.open,
    state => state.trackData.startPosition,
    state => state.trackData.label,
    state => state.trackData.version,
    state => state.playback.maxIndex
  ],
  ({track}, open, startPosition, label, version, maxIndex) => {
    let trackData = open ? {
      label,
      version,
      startPosition,
      duration: maxIndex,
      lines: track.getData()
    } : null
    let trackSaver = {
      open,
      trackData,
      label,
      fileName: label || 'untitled'
    }
    return trackSaver
  }
)

const drawingSurfaceSelector = createSelector(
  [
    state => state.drawStreamActive
  ],
  (drawStreamActive) => ({drawStreamActive})
)

const splashModalSelector = createSelector(
  [
    state => state.loadedTrack,
    state => state.releaseInfo
  ],
  (loadedTrack, releaseInfo) => ({
    initOpen: !loadedTrack,
    releaseInfo: releaseInfo.map(({name, tag_name, body, html_url, created_at}) => ({
      name: name,
      tag: tag_name,
      body,
      url: html_url,
      date: new Date(created_at).toLocaleDateString()
    }))
  })
)

export default createStructuredSelector({
  editor: editorSelector,
  fileLoader: fileLoaderSelector,
  timeline: timelineSelector,
  display: displaySelector,
  widthHeight: widthHeightSelector,
  trackSaver: trackSaverSelector,
  drawingSurface: drawingSurfaceSelector,
  splashModal: splashModalSelector,
  recorder: createStructuredSelector({
    width: () => window.recorder.width,
    height: () => window.recorder.height,
    index: (state) => state.playback.index,
    maxIndex: (state) => state.playback.maxIndex,
    fileName: (state) => state.trackData.label || 'untitled'
  }),
  recording: (state) => state.recording,
  audioFileLoader: audioFileLoaderSelector
})
