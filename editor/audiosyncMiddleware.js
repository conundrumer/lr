import unlockWebAudio from 'web-audio-ios'

import {SET_FRAME_RATE, SET_PLAYBACK_STATE, LOAD_AUDIO, SET_AUDIO_OFFSET, TOGGLE_AUDIO, audioLoadFail} from './actions'

// const FPS = 40

// adapted from http://stackoverflow.com/a/12518156/2573317
function copyAndReverseAudioBuffer (audioContext, audioBuffer) {
  let channels = []
  let numChannels = audioBuffer.numberOfChannels

  // clone and reverse the underlying Float32Arrays
  for (let i = 0; i < numChannels; i++) {
    channels[i] = new Float32Array(audioBuffer.getChannelData(i))
    channels[i].reverse()
  }

  // create the new AudioBuffer (assuming AudioContext variable is in scope)
  let newBuffer = audioContext.createBuffer(audioBuffer.numberOfChannels, audioBuffer.length, audioBuffer.sampleRate)

  // copy the cloned arrays to the new AudioBuffer
  for (let i = 0; i < numChannels; i++) {
    newBuffer.getChannelData(i).set(channels[i])
  }

  return newBuffer
}

// adapted from https://github.com/eipark/buffaudio
class AudioBufferPlayer {
  constructor (audioContext) {
    this.audioContext = audioContext
    this.buffer = null // AudioBuffer
    this.reversedBuffer = null
    this.source = null // AudioBufferSourceNode
    this.activeSource = null
    this.playbackRate = 1
    this.initialOffset = 0
  }

  setInitialOffset (offset) {
    this.initialOffset = offset
  }

  // Create a new AudioBufferSourceNode
  initSource (reversed = false) {
    this.source = this.audioContext.createBufferSource()
    this.source.buffer = reversed ? this.reversedBuffer : this.buffer
    this.source.connect(this.audioContext.destination)
    // this.source.onended = () => console.log('end of playback')
  }

  // Whenever we get a new AudioBuffer, we create a new AudioBufferSourceNode and reset
  // the playback time. Make sure any existing audio is stopped beforehand.
  initNewBuffer (buffer) {
    this.stop()
    this.buffer = buffer
    this.reversedBuffer = copyAndReverseAudioBuffer(this.audioContext, buffer)
    this.initSource()
  }

  // Stops or pauses playback and sets offset accordingly
  stop () {
    if (!this.activeSource) return
    // this.isPlaying = false // Set to flag to endOfPlayback callback that this was set manually
    this.activeSource.stop(0)
    this.activeSource = null
  }

  // Play the currently loaded buffer
  play (offset = 0, rate = 1) {
    if (this.activeSource) {
      this.stop()
    }
    let reversed = rate < 0
    offset += this.initialOffset
    offset = reversed ? this.source.buffer.duration - offset : offset
    if (offset > this.buffer.duration) {
      return
    }
    let when = this.audioContext.currentTime + Math.max(0, -offset)
    offset = Math.max(0, offset)
    this.initSource(reversed)
    this.source.playbackRate.value = Math.abs(rate)
    this.source.start(when, offset)
    this.activeSource = this.source
  }
}

window.AudioContext = window.AudioContext || window.webkitAudioContext

export default ({getState, dispatch}) => (next) => {
  let audioContext = new window.AudioContext()
  unlockWebAudio(window, audioContext, (unlocked) => {})
  if (audioContext.state === 'suspended') {
    const resume = () => {
      audioContext.resume()
      window.removeEventListener('mousedown', resume)
      window.removeEventListener('touchend', resume)
    }
    window.addEventListener('mousedown', resume)
    window.addEventListener('touchend', resume)
  }

  let player = new AudioBufferPlayer(audioContext)

  return (action) => {
    switch (action.type) {
      case LOAD_AUDIO:
        return audioContext.decodeAudioData(action.arraybuffer, (buffer) => {
          console.log('loaded audio')
          player.initNewBuffer(buffer)
          action.enabled = true
          next(action)
        }, (err) => dispatch(audioLoadFail(err)))
      case SET_AUDIO_OFFSET:
        player.setInitialOffset(action.offset)
        break
      case SET_PLAYBACK_STATE:
        if (getState().audio.enabled) {
          switch (action.state) {
            case 'play':
              player.play(getState().playback.index / window.FPS, 1)
              break
            case 'slowmo':
              player.play(getState().playback.index / window.FPS, 1 / 8)
              break
          }
        }
        break
      case SET_FRAME_RATE:
        if (getState().audio.enabled) {
          if (action.rate === 0) {
            player.stop()
          } else if (action.rate !== getState().playback.rate) {
            player.play(getState().playback.index / window.FPS, action.rate)
          }
        }
        break
      case TOGGLE_AUDIO:
        if (getState().audio.enabled) {
          player.stop()
        }
        return next(action)
    }
    return next(action)
  }
}
