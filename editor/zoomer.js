
function validateKeyframes (keyframes) {
  if (!(keyframes instanceof Array)) {
    throw new Error(`Keyframes should be an array: ${JSON.stringify(keyframes)}`)
  }
  if (keyframes.length < 1) {
    throw new Error(`Keyframes should not be empty: ${JSON.stringify(keyframes)}`)
  }

  keyframes.reduce((prevIndex, keyframe, i) => {
    let keyFrameString = JSON.stringify(keyframe)
    let id = `Keyframe #${i}`

    if (!(keyframe instanceof Array)) {
      throw new Error(`${id} should be an array: ${keyFrameString}`)
    }
    let [index, zoom] = keyframe
    if (index instanceof Array) {
      let [frames, sec = 0, min = 0] = [...index].reverse()
      switch (index.length) {
        case 3:
          if (!Number.isInteger(min)) {
            throw new Error(`${id} index minutes (${min}) should be an integer: ${keyFrameString}`)
          }
          /* falls through */
        case 2:
          if (!Number.isInteger(sec)) {
            throw new Error(`${id} index seconds (${sec}) should be an integer: ${keyFrameString}`)
          }
          /* falls through */
        case 1:
          if (!Number.isInteger(frames)) {
            throw new Error(`${id} index frames (${frames}) should be an integer: ${keyFrameString}`)
          }
          break
        case 0:
        default:
          throw new Error(`${id} index (${JSON.stringify(index)}) should be [frames], [sec,frames], or [min,sec,frames]: ${keyFrameString}`)
      }
      index = frames + 40 * (sec + 60 * min)
    }
    if (!Number.isInteger(index)) {
      throw new Error(`${id} index (${index}) should be an integer: ${keyFrameString}`)
    }
    if (index <= prevIndex) {
      throw new Error(`${id} index (${index}) should be greater than the previous index (${prevIndex}): ${keyFrameString}`)
    }
    if (!Number.isFinite(zoom)) {
      throw new Error(`${id} zoom (${zoom}) should be a number: ${keyFrameString}`)
    }
    if (i === 0 && index !== 0) {
      throw new Error(`The first keyframe index (${index}) should be 0: ${keyFrameString}`)
    }
    return index
  }, -1)
}

/**
 * Linear interpolation between (x0, y0) and (x1, y1)
 * @param  {int} x0
 * @param  {int} x1
 * @param  {float} y0
 * @param  {float} y1
 * @return {Array} the values between x0 and x1, EXCLUSIVE
 */
function interpolate (x0, x1, y0, y1) {
  let dx = x1 - x0
  let dy = y1 - y0
  let m = dy / dx
  let values = []
  for (let x = 1; x < dx; x++) {
    values.push(m * x + y0)
  }
  return values
}

/**
 * hann filter of length 2 * smoothing + 1
 * @param  {Array} t         [description]
 * @param  {int} smoothing [description]
 * @return {[type]}           [description]
 */
function smooth (x, smoothing) {
  if (smoothing === 0) {
    return x
  }
  x = [...x, ...Array(smoothing).fill(x[x.length - 1])]
  let i_0 = smoothing
  let n = smoothing + 1 + smoothing
  let f = Array(n).fill().map((_, i) => Math.cos(Math.PI * (i - i_0) / n))
  let sum = f.reduce((s, f_i) => s + f_i, 0)
  f = f.map(f_i => f_i / sum)

  let clamp = (i) => Math.max(0, Math.min(x.length - 1, i))

  return x.map((x_j, j) =>
    f.reduce((sum, f_i, i) =>
      sum + f_i * x[clamp(j + i - i_0)]
    , 0)
  )
}

export default function createZoomer (keyframes, smoothing = 20) {
  validateKeyframes(keyframes)
  if (!Number.isInteger(smoothing) || smoothing < 0) {
    throw new Error(`Smoothing should be a positive integer: ${smoothing}`)
  }

  let [[, firstZoom], ...rest] = keyframes

  let zooms = rest.reduce((zooms, [index, zoom], i) => {
    let prevIndex = zooms.length - 1
    let prevZoom = zooms[prevIndex]
    if (index instanceof Array) {
      let [frames, sec = 0, min = 0] = [...index].reverse()
      index = frames + 40 * (sec + 60 * min)
    }
    return [...zooms, ...interpolate(prevIndex, index, prevZoom, zoom), zoom]
  }, [firstZoom])

  zooms = smooth(zooms, smoothing)

  return (index) => Math.pow(2, zooms[Math.min(index, zooms.length - 1)])
}

createZoomer.help = `
Usage: getAutoZoom = createZoomer(keyframes, [smoothing])

keyframes:
[
  [0, zoom0],
  [index1, zoom1],
  [[seconds, frames], zoom2],
  [[minutes, seconds, frames], zoom3],
  keyframe4,
  ...
]

Example:
getAutoZoom = createZoomer([[0, 0], [[2,0], 0], [[3,0], -4], [[4,0],-4], [[4,1],-1]])

Example with no smoothing:
getAutoZoom = createZoomer([[0, 0], [40, 0], [60, -1], [80, -1], [81, 0]], 0)
`
