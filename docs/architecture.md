# Architecture of Line Rider

### Preface
This document is being written as I'm starting to look at the codebase and make
changes for the first time, and I figured that as I'm learning how it all works
it would make sense to document it for the benefit of anyone else who ends up
looking at the codebase. (Also because I'm forgetful and want a reference for
myself)

## Dive Down
Before I can do much else, I need to dive into the code from the entry point and
get an idea of what's going on. Here's what I found:

### assets/index.html
`assets/index.html` is where we start, with a reference to `editor/main.js` that
actually has most of the startup code. The rest of `assets/index.html` is just
meta tags, a mountpoint in the DOM and some analytics.

### editor/main.js
This starts out with approximately one millions requires and imports, but
quickly gets to the interesting bits. Because we can run Line Rider with all
sorts of debugging magic attached, there's no freestanding call to
ReactDOM.render(). Instead, that's wrapped in a render() function - normally
it'll just wrap the <App> component (found in `editor/components/App.js`) in the
usual redux <Provider>.

> #### What are providers?
> You might want to look at http://redux.js.org/docs/basics/UsageWithReact.html to
> understand what's happening, but in essence you just define a store to props
> mapping, and then use the connect() function from redux to combine that mapping
> with your component, resulting in a magical component that's now aware of your
> stores without explicitly asking for them (as they come via props).

If we're in the `__DEVTOOLS__` mode (towards the bottom of the file), we replace
the basic render() function with one that includes a `DebugPanel` from the
`redux-devtools` library. We also extend the store middlewares (or enhancers as
Redux likes to call them) with the `persistState` enhancer to persist the stores
between page reloads.

The app's stores are set up by wrapping redux's `createStore` function in one
that will apply the middlewares, and we then create the actual store by calling
this wrapper with the results of redux's `combineReducers` function. The actual
reducers are kept in `editor/reducers.js`.

### editor/reducers.js
Imports in the action types from `editor/actions.js` and defines reducing
functions for various parts of the app's state based on these actions. This
could perhaps use some factoring out into other files.

### editor/actions.js
Exports a bunch of functions (action creators) that return action objects.
There's also a lot of "thunks" (if you're as confused as I was about this, look
at http://redux.js.org/docs/advanced/AsyncActions.html) that return functions
instead.

Thunks are used here to:

  - Dispatch actions in response to XHR responses
  - Condtionally dispatch actions (i.e the `deleteSelection` thunk will only
    dispatch `REMOVE_LINE` actions if there's a selection to delete)
  - Create side effects that don't have anywhere else to go (i.e hotkeys)
  - Dispatch actions that are based on the current state (for example `setCam`
    needs to know about existing state so that it knows what coordinates to use
    based on whether the camera lock is enabled)

Every single thing that can cause a change to the app's state and interface is
done via an action, and again there could probably be some factoring out into
other files done here.

### editor/bindHotkey.js
This file exports a single function that's used within the `setHotkey` thunk in
`editor/actions.js`, and nowhere else. Strangely, while this file is used to
bind new hotkeys, it's the `setHotkey` thunk that's responsible for unbinding
them when they need to change.

`bindHotkey` will look into the buttons as defined in `editor/buttons.js` and
see if the action for that hotkey matches. If it does, we assign it to that
action - if it doesn't a null assignment is made.

Additionally, every single call to `bindHotkey` will go and establish
unchangeable bindings for the modifier keys, number keys, spacebar and backspace
key. When the defaults hotkeys for buttons are established at startup, this also
results in establishing hotkeys for those other keys.

#### ripples
Something that's a concept to hotkey binding here that I'd never heard of is
"rippling". From what I can see, ripples are bits of UI that can trigger when
a hotkey is pressed, which come from `editor/components/IconButton.js`.

### editor/buttons.js
Defines buttons, which have actions, names, hotkeys and icons. Also defines the
menus and button groups (menu items are also buttons even though they aren't
shown in the toolbar in the same way as other buttons).

Doesn't actually define the UI components or anything else.

### editor/components/App.js
