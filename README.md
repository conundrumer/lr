Line Rider JS
======

How to run:
```sh
npm install
npm run get-fonts
npm run start -- [--prod] [--devtools]
```
- `--prod`: Run in production environment, which turns off React debugging and speeds things up
- `--devtools`: Uses redux-devtools to analyze actions and state

How to build:
```sh
npm run build
```
Builds to `dist`.
